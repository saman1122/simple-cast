package com.hencky.simplecast.upnp;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.cast.framework.Session;
import com.hencky.simplecast.model.Device;

/**
 * Represents an UPnP session with a receiver application.
 */
public class UpnpSession extends Session {

    private final String mSessionId;
    private Device mDevice;

    public UpnpSession(Context applicationContext, String category, String sessionId) {
        super(applicationContext, category, sessionId);
        mSessionId = sessionId;
    }

    @Override
    protected void onStarting(Bundle routeInfoExtras) {
        mDevice = Device.getFromBundle(routeInfoExtras);
    }

    @Override
    protected void onResuming(Bundle routeInfoExtras) {
        mDevice = Device.getFromBundle(routeInfoExtras);
    }

    @Override
    protected void start(Bundle routeInfoExtra) {
        mDevice = Device.getFromBundle(routeInfoExtra);
        if (mDevice == null) {
            notifyFailedToStartSession(8);
        } else {
            notifySessionStarted(mSessionId);
        }
    }

    @Override
    protected void resume(Bundle routeInfoExtra) {
        mDevice = Device.getFromBundle(routeInfoExtra);
        if (mDevice == null) {
            notifyFailedToResumeSession(8);
        } else {
            notifySessionResumed(false);
        }
    }

    @Override
    protected void end(boolean stopCasting) {
        notifySessionEnded(0);
    }

    public Device getDevice() {
        return mDevice;
    }
}
