package com.hencky.simplecast.upnp;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;

import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.utils.LogHelper;

import java.util.HashSet;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.mediarouter.media.MediaControlIntent;
import androidx.mediarouter.media.MediaRouteDescriptor;
import androidx.mediarouter.media.MediaRouteProvider;
import androidx.mediarouter.media.MediaRouteProviderDescriptor;
import androidx.mediarouter.media.MediaRouter;

/**
 * Media route provider for UPnP devices.
 */
public class UpnpRouteProvider extends MediaRouteProvider {
    private static final String TAG = LogHelper.makeLogTag(UpnpRouteProvider.class);

    private static UpnpRouteProvider INSTANCE;

    /**
     * A custom media control intent category for requests that are supported
     * by this provider's routes.
     */
    public static final String CATEGORY_UPNP = AppKeys.APP_PACKAGE_NAME + ".CATEGORY_UPNP";

    private IntentFilter mPlaybackFilter;
    private Set<Device> mDevices;

    /**
     * Creates a {@link MediaRouteProvider} for UPnP devices.
     *
     * @param context The context that is used.
     */
    private UpnpRouteProvider(Context context) {
        super(context);
        init();
    }

    public static synchronized UpnpRouteProvider getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UpnpRouteProvider(context.getApplicationContext());
        }
        return INSTANCE;
    }

    /**
     * Initializes what is needed when creating an instance of this class.
     */
    private void init() {
        mDevices = new HashSet<>();
        mPlaybackFilter = new IntentFilter();
        mPlaybackFilter.addCategory(CATEGORY_UPNP);
        mPlaybackFilter.addAction(MediaControlIntent.ACTION_PLAY);
        mPlaybackFilter.addAction(MediaControlIntent.ACTION_SEEK);
        mPlaybackFilter.addAction(MediaControlIntent.ACTION_PAUSE);
        mPlaybackFilter.addAction(MediaControlIntent.ACTION_RESUME);
        mPlaybackFilter.addAction(MediaControlIntent.ACTION_STOP);
        mPlaybackFilter.addDataScheme("http");
        addDataTypeUnchecked(mPlaybackFilter, "video/*");
    }

    private void addDataTypeUnchecked(IntentFilter filter, String type) {
        try {
            filter.addDataType(type);
        } catch (IntentFilter.MalformedMimeTypeException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Adds a route to a {@link Device}.
     *
     * @param device the device to add
     */
    synchronized void addDevice(final Device device) {
        if (mDevices.contains(device)) {
            LogHelper.w(TAG, "The route for device " + device.getIdentifier() + " (" +
                    device.getName() + ") has already been added.");
            return;
        }
        mDevices.add(device);
        publishRoutes();
    }

    /**
     * Removes a route to a {@link Device}.
     *
     * @param device the Device to remove
     */
    synchronized void removeDevice(final Device device) {
        if (mDevices.contains(device)) {
            mDevices.remove(device);
            publishRoutes();
        }
    }

    /**
     * Checks whether this provider contains a given {@link Device}.
     *
     * @param device The device to check
     * @return {@code true} if this provider contains the given {@link Device},
     * otherwise {@code false}
     */
    public boolean providesDevice(final Device device) {
        return mDevices.contains(device);
    }

    /**
     * Builds a {@link MediaRouteDescriptor} for a given {@link Device}.
     *
     * @param device the Device for which a MediaRouteDescriptor is returned
     * @return the MediaRouteDescriptor
     */
    private MediaRouteDescriptor buildMediaRouteDescriptor(Device device) {
        Bundle extras = new Bundle();
        extras.putParcelable(AppKeys.EXTRA_DEVICE, device);

        // Create a route descriptor using previously created IntentFilters
        return new MediaRouteDescriptor.Builder(
                device.getIdentifier(),
                device.getName())
                .setDescription("Route to " + device.getName())
                .addControlFilter(mPlaybackFilter)
                //.setPlaybackStream(AudioManager.STREAM_MUSIC)
                .setPlaybackType(MediaRouter.RouteInfo.PLAYBACK_TYPE_REMOTE)
                //.setVolumeHandling(MediaRouter.RouteInfo.PLAYBACK_VOLUME_VARIABLE)
                //.setVolumeMax(VOLUME_MAX)
                //.setVolume(mVolume)
                .setExtras(extras)
                .build();
    }

    /**
     * Publishes the routes to the framework.
     */
    private void publishRoutes() {
        if (!mDevices.isEmpty()) {
            LogHelper.d(TAG, "Publishing routes for " + mDevices);
            MediaRouteProviderDescriptor.Builder providerDescriptorBuilder =
                    new MediaRouteProviderDescriptor.Builder();

            for (Device device : mDevices) {
                providerDescriptorBuilder.addRoute(buildMediaRouteDescriptor(device));
            }

            // Publish the descriptor to the framework
            setDescriptor(providerDescriptorBuilder.build());
        }
    }

    /**
     * Releases resources.
     */
    public void release() {
        LogHelper.d(TAG, "Release resources");
        INSTANCE = null;
    }
}
