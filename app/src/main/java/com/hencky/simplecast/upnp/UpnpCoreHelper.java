package com.hencky.simplecast.upnp;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.ModelUtils;
import com.hencky.simplecast.utils.UpnpUtils;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceConfiguration;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.android.AndroidRouter;
import org.fourthline.cling.android.AndroidUpnpServiceConfiguration;
import org.fourthline.cling.controlpoint.ActionCallback;
import org.fourthline.cling.controlpoint.ControlPoint;
import org.fourthline.cling.controlpoint.SubscriptionCallback;
import org.fourthline.cling.model.UnsupportedDataException;
import org.fourthline.cling.model.gena.CancelReason;
import org.fourthline.cling.model.gena.GENASubscription;
import org.fourthline.cling.model.gena.RemoteGENASubscription;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.meta.Device;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.model.meta.StateVariable;
import org.fourthline.cling.model.meta.StateVariableTypeDetails;
import org.fourthline.cling.model.state.StateVariableValue;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.ServiceType;
import org.fourthline.cling.model.types.UDAServiceId;
import org.fourthline.cling.model.types.UDAServiceType;
import org.fourthline.cling.model.types.UDN;
import org.fourthline.cling.protocol.ProtocolFactory;
import org.fourthline.cling.registry.DefaultRegistryListener;
import org.fourthline.cling.registry.Registry;
import org.fourthline.cling.registry.RegistryListener;
import org.fourthline.cling.support.model.SeekMode;
import org.fourthline.cling.transport.Router;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides facilities to interact with Cling Core framework.
 */
public final class UpnpCoreHelper {

    private static final String TAG = LogHelper.makeLogTag(UpnpCoreHelper.class);

    private static final int DEFAULT_SUBSCRIPTION_DURATION_SECONDS = 600;

    private static UpnpCoreHelper INSTANCE;

    private final UpnpRouteProvider mUpnpRouteProvider;
    private final Map<String, SubscriptionCallback> mSubscriptionCallbacks;
    private final Map<com.hencky.simplecast.model.Device, Boolean> mRelTimeSeekModeCapability;
    private final Map<com.hencky.simplecast.model.Device, Boolean> mSetMuteCapability;

    private Context mAppContext;
    private UpnpService mUpnpService;
    private UpnpRegistryListener mUpnpRegistryListener;
    private Service mAVTransportService;
    private Service mConnectionManagerService;
    private Service mRenderingControlService;
    private com.hencky.simplecast.model.Device mDevice;

    private UpnpCoreHelper(Context appContext) {
        mAppContext = appContext;
        mUpnpService = createUpnpService();
        mUpnpRegistryListener = new UpnpRegistryListener();
        getRegistry().addListener(mUpnpRegistryListener);
        mUpnpRouteProvider = UpnpRouteProvider.getInstance(mAppContext);
        mSubscriptionCallbacks = new HashMap<>(3);
        mRelTimeSeekModeCapability = new HashMap<>(5);
        mSetMuteCapability = new HashMap<>(5);
    }

    public static synchronized UpnpCoreHelper getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UpnpCoreHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    public Service getAVTransportService() {
        return mAVTransportService;
    }

    public Service getConnectionManagerService() {
        return mConnectionManagerService;
    }

    public Service getRenderingControlService() {
        return mRenderingControlService;
    }

    /**
     * @return {@code true} if the selected device has the REL_TIME Seek Mode capability,
     * otherwise {@code false}
     */
    public boolean hasRelTimeSeekModeCapability() {
        if (mDevice == null)
            return false;
        if (mRelTimeSeekModeCapability.containsKey(mDevice)) {
            return mRelTimeSeekModeCapability.get(mDevice);
        } else {
            return false;
        }
    }

    /**
     * @return {@code true} if the selected device has the SetMute capability,
     * otherwise {@code false}
     */
    public boolean hasSetMuteCapability() {
        if (mDevice == null)
            return false;
        if (mSetMuteCapability.containsKey(mDevice)) {
            return mSetMuteCapability.get(mDevice);
        } else {
            return false;
        }
    }

    /**
     * Creates a shared configuration data of the UPnP stack.
     *
     * @return the configuration
     */
    private UpnpServiceConfiguration createConfiguration() {
        return new AndroidUpnpServiceConfiguration() {
            @Override
            public ServiceType[] getExclusiveServiceTypes() {
                return new ServiceType[]{
                        new UDAServiceType("AVTransport"),
                        new UDAServiceType("ConnectionManager"),
                        new UDAServiceType("RenderingControl")
                };
            }
        };
    }

    /**
     * Creates an {@link AndroidRouter}.
     *
     * @param configuration   The shared configuration data of the UPnP stack.
     * @param protocolFactory The factory for UPnP protocols.
     * @param context         The context that is used.
     * @return the AndroidRouter
     */
    private AndroidRouter createRouter(UpnpServiceConfiguration configuration,
                                       ProtocolFactory protocolFactory,
                                       Context context) {
        return new AndroidRouter(configuration, protocolFactory, context);
    }

    /**
     * Creates an {@link UpnpService}.
     *
     * @return an UpnpService
     */
    private UpnpService createUpnpService() {

        UpnpServiceConfiguration configuration = createConfiguration();

        return new UpnpServiceImpl(configuration) {

            @Override
            protected Router createRouter(ProtocolFactory protocolFactory, Registry registry) {
                return UpnpCoreHelper.this.createRouter(configuration, protocolFactory, mAppContext);
            }

            @Override
            public synchronized void shutdown() {
                // First have to remove the receiver, so Android won't complain about it leaking
                // when the main UI thread exits.
                ((AndroidRouter) getRouter()).unregisterBroadcastReceiver();

                // Now we can concurrently run the Cling shutdown code, without occupying the
                // Android main UI thread. This will complete probably after the main UI thread
                // is done.
                super.shutdown(true);
            }
        };
    }

    private Registry getRegistry() {
        return mUpnpService.getRegistry();
    }

    public ControlPoint getControlPoint() {
        return mUpnpService.getControlPoint();
    }

    /**
     * Resumes background maintenance (thread) of registered items.
     */
    public void resumeRegistry() {
        getRegistry().resume();
    }

    /**
     * Stops background maintenance (thread) of registered items.
     */
    public void pauseRegistry() {
        getRegistry().pause();
    }

    /**
     * Searches UPnP control points.
     */
    public void searchControlPoints() {
        getControlPoint().search();
    }

    /**
     * Executes asynchronously an action.
     *
     * @param callback the callback associated to the action.
     */
    public void executeAction(ActionCallback callback) {
        getControlPoint().execute(callback);
    }

    /**
     * Executes asynchronously an event subscription.
     *
     * @param callback the callback associated to the event subscription.
     */
    private void executeSubscription(SubscriptionCallback callback) {
        getControlPoint().execute(callback);
    }

    /**
     * Initializes resources for a given {@link com.hencky.simplecast.model.Device}.
     *
     * @param device The device for which the resources are initialized.
     */
    public void initialize(com.hencky.simplecast.model.Device device) {
        findServicesByDevice(device);
        subscribeToServicesEventsIfNeeded();
    }

    /**
     * Finds UPnP services for a given {@link com.hencky.simplecast.model.Device}.
     *
     * @param device the device that is used to search the UPnP services
     */
    private void findServicesByDevice(com.hencky.simplecast.model.Device device) {
        mDevice = device;

        mAVTransportService = findServiceByDevice(new UDAServiceId("AVTransport"), device);
        if (mAVTransportService == null) {
            LogHelper.w(TAG, "AVTransport service was not found.");
        } else {
            LogHelper.d(TAG, "Found AVTransport service: " +
                    mAVTransportService.toString());
        }

        mConnectionManagerService = findServiceByDevice(new UDAServiceId("ConnectionManager"),
                device);
        if (mConnectionManagerService == null) {
            LogHelper.w(TAG, "ConnectionManager service was not found.");
        } else {
            LogHelper.d(TAG, "Found ConnectionManager service: " +
                    mConnectionManagerService.toString());

        }

        mRenderingControlService = findServiceByDevice(new UDAServiceId("RenderingControl"),
                device);
        if (mRenderingControlService == null) {
            LogHelper.w(TAG, "RenderingControl service was not found.");
        } else {
            LogHelper.d(TAG, "Found RenderingControl service: " +
                    mRenderingControlService.toString());
        }
    }

    /**
     * Finds an UPnP service for a given {@link ServiceId}
     * and a {@link com.hencky.simplecast.model.Device}.
     *
     * @param serviceId the service identifier that is used to find the service
     * @param device    the device that is used to search the UPnP service
     */
    private org.fourthline.cling.model.meta.Service findServiceByDevice(
            ServiceId serviceId,
            com.hencky.simplecast.model.Device device) {

        if (!DeviceType.UPNP.equals(device.getDeviceType()))
            return null;

        String udn = device.getIdentifier();

        // Get the UPnP device for the given unique device name
        final org.fourthline.cling.model.meta.Device remoteDevice =
                getRegistry().getRemoteDevice(new UDN(udn), true);
        if (remoteDevice == null) {
            LogHelper.e(TAG, "No remote device " + device.getIdentifier() +
                    " found on service " + serviceId.getId());
            return null;
        }

        // Search the service on the remote device
        LogHelper.d(TAG, "Search service " + serviceId.getId() + " on remote device " +
                remoteDevice.getDisplayString());
        org.fourthline.cling.model.meta.Service service = remoteDevice.findService(serviceId);
        if (service == null) {
            LogHelper.w(TAG, "Service " + serviceId.getId() + " was not found.");
        } else {
            LogHelper.d(TAG, "Found service: " + service.toString());
            if ("AVTransport".equals(service.getServiceId().getId())) {
                // Case when service is AVTransport
                // Verify if REL_TIME seek mode is supported by AVTransport service
                boolean relTimeSeekModeCapability = false;
                if (service.getAction("Seek") == null) {
                    LogHelper.w(TAG, "Seek action was not found in AVTransport service" +
                            " for device " + remoteDevice.getDetails().getFriendlyName());
                } else {
                    StateVariable stateVariable = service.getStateVariable("A_ARG_TYPE_SeekMode");
                    if (stateVariable == null) {
                        LogHelper.w(TAG, "StateVariable A_ARG_TYPE_SeekMode was not" +
                                " found in AVTransport service for device " +
                                remoteDevice.getDetails().getFriendlyName());
                    } else {
                        StateVariableTypeDetails typeDetails = stateVariable.getTypeDetails();
                        if (typeDetails != null) {
                            String[] allowedValues = typeDetails.getAllowedValues();
                            if (allowedValues == null) {
                                LogHelper.w(TAG, "No allowed values were found in" +
                                        " AVTransport service for device " +
                                        remoteDevice.getDetails().getFriendlyName() +
                                        " and StateVariable A_ARG_TYPE_SeekMode.");
                            } else {
                                for (String allowedValue : allowedValues) {
                                    if (SeekMode.REL_TIME.name().equals(allowedValue)) {
                                        relTimeSeekModeCapability = true;
                                    }
                                }
                            }
                        }
                    }
                }
                mRelTimeSeekModeCapability.put(device, relTimeSeekModeCapability);
            } else if ("RenderingControl".equals(service.getServiceId().getId())) {
                // Case when service is RenderingControl
                // Verify if SetMute action is supported by RenderingControl service
                boolean setMuteCapability = false;
                if (service.getAction("SetMute") == null) {
                    LogHelper.w(TAG,
                            "SetMute action was not found in RenderingControl service" +
                                    " for device " + remoteDevice.getDetails().getFriendlyName());
                } else {
                    setMuteCapability = true;
                    // Verify if Mute state variable is supported by RenderingControl service
                    StateVariable muteStateVariable = service.getStateVariable("Mute");
                    if (muteStateVariable == null) {
                        LogHelper.w(TAG, "StateVariable Mute was not" +
                                " found in RenderingControl service for device " +
                                remoteDevice.getDetails().getFriendlyName());
                        setMuteCapability = false;
                    }
                }
                mSetMuteCapability.put(device, setMuteCapability);
            }
        }
        return service;
    }

    /**
     * Subscribes to events from UPnP services if needed.
     */
    private void subscribeToServicesEventsIfNeeded() {
        subscribeToServiceEventsIfNeeded(getAVTransportService());
        subscribeToServiceEventsIfNeeded(getConnectionManagerService());
        subscribeToServiceEventsIfNeeded(getRenderingControlService());
    }

    /**
     * Subscribes to events from a given {@link org.fourthline.cling.model.meta.Service} if needed.
     *
     * @param service the Service to subscribe
     */
    private void subscribeToServiceEventsIfNeeded(org.fourthline.cling.model.meta.Service service) {
        if (service == null)
            return;

        if (mSubscriptionCallbacks.get(service.getServiceId().getId()) != null)
            return;

        SubscriptionCallback callback = new SubscriptionCallback(service,
                DEFAULT_SUBSCRIPTION_DURATION_SECONDS) {

            @Override
            public void established(GENASubscription sub) {
                LogHelper.d(TAG, "Established subscription " + sub.getSubscriptionId() +
                        " on service " + service.getServiceId());
                mSubscriptionCallbacks.put(service.getServiceId().getId(), this);
            }

            @Override
            protected void failed(GENASubscription subscription, UpnpResponse responseStatus,
                                  Exception exception, String defaultMsg) {
                LogHelper.e(TAG, "Failed subscribing to service " +
                        service.getServiceId() + ": " + defaultMsg);
            }

            @Override
            public void ended(GENASubscription sub, CancelReason reason, UpnpResponse response) {
                LogHelper.d(TAG, "Ended subscription " + sub.getSubscriptionId() +
                        " on service " + service.getServiceId() + ", reason: " + reason);
                mSubscriptionCallbacks.remove(service.getServiceId().getId());
            }

            @Override
            public void eventReceived(GENASubscription sub) {
                LogHelper.d(TAG, "Event for subscription " + sub.getSubscriptionId() +
                        " on service " + service.getServiceId() + ": " +
                        sub.getCurrentSequence().getValue());

                Map values = sub.getCurrentValues();
                for (Object key : values.keySet()) {
                    StateVariableValue value = (StateVariableValue) values.get(key);
                    if (value != null) {
                        LogHelper.d(TAG, key + ": " + value.toString());
                        if ("LastChange".equals(value.getStateVariable().getName()) &&
                                value.getValue() != null) {
                            String lastChangeString = (String) value.getValue();
                            UpnpUtils.handleLastChangeEvent(mAppContext, lastChangeString);
                        }
                    }
                }
            }

            @Override
            public void eventsMissed(GENASubscription sub, int numberOfMissedEvents) {
                LogHelper.d(TAG, "Missed events for subscription " +
                        sub.getSubscriptionId() + ": " + numberOfMissedEvents);
            }

            @Override
            protected void invalidMessage(RemoteGENASubscription sub, UnsupportedDataException ex) {
                LogHelper.e(TAG, "Invalid message for subscription " +
                        sub.getSubscriptionId() + ": " + ex.getMessage());
            }
        };

        executeSubscription(callback);
    }

    /**
     * Releases resources.
     */
    public void release() {
        LogHelper.d(TAG, "Release resources");
        if (getRegistry() != null) {
            getRegistry().removeListener(mUpnpRegistryListener);
        }
        mUpnpRegistryListener = null;
        if (mUpnpService != null) {
            mUpnpService.shutdown();
        }
        mUpnpService = null;
        mAppContext = null;
        INSTANCE = null;
    }

    /**
     * Represents a {@link RegistryListener} for UPnP devices.
     */
    private class UpnpRegistryListener extends DefaultRegistryListener {

        @Override
        public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice device) {
            LogHelper.d(TAG, "Started discovering remote device " +
                    device.getDisplayString());
        }

        @Override
        public void remoteDeviceDiscoveryFailed(Registry registry, RemoteDevice device, Exception ex) {
            LogHelper.e(TAG, "Failed discovering remote device " +
                    device.getDisplayString() + ": " + ex.getMessage());
        }

        @Override
        public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
            // Only add MediaRenderer device type
            if ("MediaRenderer".equals(device.getType().getType())) {
                deviceAdded(registry, device);
            }
        }

        @Override
        public void deviceAdded(Registry registry, Device device) {
            LogHelper.d(TAG, "deviceAdded - " + device);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> mUpnpRouteProvider.addDevice(ModelUtils.toDevice(device)));
        }

        @Override
        public void deviceRemoved(Registry registry, Device device) {
            LogHelper.d(TAG, "deviceRemoved - " + device);
            new Handler(Looper.getMainLooper()).post(() ->
                    mUpnpRouteProvider.removeDevice(ModelUtils.toDevice(device)));
        }
    }
}
