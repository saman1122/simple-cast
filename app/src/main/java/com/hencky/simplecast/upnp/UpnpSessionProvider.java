package com.hencky.simplecast.upnp;

import android.content.Context;

import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionProvider;

import java.util.UUID;

/**
 * Performs UpnP session construction.
 */
public class UpnpSessionProvider extends SessionProvider {
    private final Context mAppContext;

    public UpnpSessionProvider(Context context) {
        super(context, UpnpRouteProvider.CATEGORY_UPNP);
        mAppContext = context.getApplicationContext();
    }

    @Override
    public Session createSession(String sessionId) {
        if (sessionId == null) {
            sessionId = UUID.randomUUID().toString();
        }
        return new UpnpSession(mAppContext, UpnpRouteProvider.CATEGORY_UPNP, sessionId);
    }

    @Override
    public boolean isSessionRecoverable() {
        return true;
    }
}
