package com.hencky.simplecast.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.playback.UpnpPlayback;
import com.hencky.simplecast.ui.MediaBrowserActivity;
import com.hencky.simplecast.ui.UpnpImageControllerActivity;
import com.hencky.simplecast.ui.UpnpVideoControllerActivity;
import com.hencky.simplecast.upnp.UpnpSession;
import com.hencky.simplecast.utils.DrawableUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

import java.io.File;

public class UpnpMiniControllerFragment extends Fragment {

    private static final String TAG = LogHelper.makeLogTag(UpnpMiniControllerFragment.class);

    private Context mContext;
    private CastContext mCastContext;
    private UpnpSession mUpnpSession;
    private View mMiniControllerView;
    private TextView mTitleView;
    private ImageView mMiniControllerButton;
    private ProgressBar mLoadingIndicator;
    private ProgressBar mProgressBar;
    private MediaItem mMediaItem;

    private long mPlaybackTimeSeconds = 0;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;

    private final SessionManagerListener<UpnpSession> mUpnpSessionManagerListener = new
            UpnpSessionManagerListenerImpl();

    /**
     * Broadcast receiver for tracking various events.
     */
    private final BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (MediaBrowserActivity.ACTION_MEDIA_ITEM_SELECTED.equals(action)) {
                    mLoadingIndicator.setVisibility(View.VISIBLE);
                    mMediaItem = intent.getParcelableExtra(AppKeys.EXTRA_MEDIA_ITEM);
                } else if (UpnpPlayback.ACTION_AVTRANSPORT_SETAVTRANSPORTURI.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(AppKeys
                                    .EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received SetAVTransportURI action invocation " +
                            "result: " + result.name());
                    mLoadingIndicator.setVisibility(View.VISIBLE);
                    if (ActionInvocationResult.SUCCESS.equals(result)) {
                        setupMiniController();
                        mMiniControllerView.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    };

    /**
     * Broadcast receiver for tracking media events such as
     * changed media player state,
     * or obtained playback time.
     */
    private final BroadcastReceiver mMediaEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);

            if (intent == null)
                return;

            String action = intent.getAction();

            if (MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED.equals(action) ||
                    MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED.equals(action)) {
                mMediaPlayerState = (MediaPlayerState) intent.getSerializableExtra(
                        AppKeys.EXTRA_PLAYER_STATE);
                LogHelper.d(TAG, "Media player state: " + mMediaPlayerState);
                DeviceType deviceType = (DeviceType) intent.getSerializableExtra(
                        AppKeys.EXTRA_DEVICE_TYPE);
                if (!DeviceType.UPNP.equals(deviceType)) {
                    LogHelper.d(TAG, "Ignoring media player state for device type " +
                            deviceType.name());
                    return;
                }
                if (MediaPlayerState.PLAYING.equals(mMediaPlayerState)) {
                    mMiniControllerButton.setVisibility(View.VISIBLE);
                    setControllerButtonState();
                    mLoadingIndicator.setVisibility(View.INVISIBLE);
                } else if (MediaPlayerState.PAUSED.equals(mMediaPlayerState)) {
                    mMiniControllerButton.setVisibility(View.VISIBLE);
                    setControllerButtonState();
                    mLoadingIndicator.setVisibility(View.INVISIBLE);
                } else if (MediaPlayerState.NONE.equals(mMediaPlayerState) ||
                        MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
                    if (mMediaItem instanceof VideoItem) {
                        mPlaybackTimeSeconds = 0;
                    }
                    resetMiniController();
                    mLoadingIndicator.setVisibility(View.INVISIBLE);
                    mMiniControllerView.setVisibility(View.GONE);
                } else if (MediaPlayerState.TRANSITIONING.equals(mMediaPlayerState)) {
                    mLoadingIndicator.setVisibility(View.VISIBLE);
                }
            } else if (MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED.equals(action)) {
                mPlaybackTimeSeconds = intent.getLongExtra(AppKeys.EXTRA_PLAYBACK_TIME, 0);
                LogHelper.d(TAG, "Obtained playback time: " + mPlaybackTimeSeconds);
                mProgressBar.setProgress((int) mPlaybackTimeSeconds);
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        mContext = null;
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        registerBroadcastReceivers();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mMiniControllerView = inflater.inflate(R.layout.fragment_upnp_mini_controller, container,
                false);

        mMiniControllerView.setOnClickListener(view -> {
            Class<?> clazz = null;
            if (mMediaItem != null) {
                if (mMediaItem instanceof ImageItem) {
                    clazz = UpnpImageControllerActivity.class;
                } else if (mMediaItem instanceof VideoItem) {
                    clazz = UpnpVideoControllerActivity.class;
                }
                if (mUpnpSession != null) {
                    Intent intent = new Intent(mContext, clazz);
                    intent.putExtra(AppKeys.EXTRA_DEVICE, mUpnpSession.getDevice());
                    intent.putExtra(AppKeys.EXTRA_MEDIA_ITEM, mMediaItem);
                    startActivity(intent);
                }
            }
        });

        mTitleView = mMiniControllerView.findViewById(R.id.upnp_mini_controller_title_view);

        mLoadingIndicator = mMiniControllerView.findViewById(
                R.id.upnp_mini_controller_loading_indicator);

        mProgressBar = mMiniControllerView.findViewById(R.id.upnp_mini_controller_progress_bar);
        mProgressBar.setProgress((int) mPlaybackTimeSeconds);

        mMiniControllerButton = mMiniControllerView.findViewById(R.id.upnp_mini_controller_button);
        mMiniControllerButton.setOnClickListener(view -> {
            if (mMediaItem instanceof ImageItem) {
                MediaPlaybackUtils.stopPlayback(mContext);
            } else if (mMediaItem instanceof VideoItem) {
                if (MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState)) {
                    MediaPlaybackUtils.pausePlayback(mContext);
                } else {
                    MediaPlaybackUtils.startPlayback(mContext);
                }
            }
        });

        return mMiniControllerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCastContext = CastContext.getSharedInstance(mContext);

        if (mUpnpSession != null && MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
            setupMiniController();
            setControllerButtonState();
            mLoadingIndicator.setVisibility(View.INVISIBLE);
            mMiniControllerButton.setVisibility(View.VISIBLE);
            mMiniControllerView.setVisibility(View.VISIBLE);
        } else {
            mMiniControllerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mCastContext.getSessionManager().addSessionManagerListener(mUpnpSessionManagerListener,
                UpnpSession.class);
        if (mUpnpSession != null) {
            LogHelper.d(TAG, "Requesting media player state");
            MediaPlaybackUtils.requestMediaPlayerState(mContext);
        }
    }

    @Override
    public void onPause() {
        mCastContext.getSessionManager().removeSessionManagerListener(
                mUpnpSessionManagerListener, UpnpSession.class);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    /**
     * Sets up the mini controller view.
     */
    private void setupMiniController() {
        if (mMediaItem == null)
            return;
        if (mMediaItem instanceof VideoItem) {
            VideoItem videoItem = (VideoItem) mMediaItem;
            mProgressBar.setMax((int) (videoItem.getDuration() / 1000));
        }
        setMediaTitle(mMediaItem.getTitle());
        loadThumbnail();
    }

    /**
     * Resets the mini controller view.
     */
    private void resetMiniController() {
        if (mMediaItem == null)
            return;
        if (mMediaItem instanceof VideoItem) {
            mProgressBar.setProgress(0);
            mProgressBar.setMax(0);
        }
        setMediaTitle(null);
        Glide.with(mContext)
                .load(R.drawable.cast_album_art_placeholder)
                .apply(RequestOptions.centerCropTransform())
                .into((ImageView) mMiniControllerView.findViewById(
                        R.id.upnp_mini_controller_icon_view));
        mMiniControllerButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Sets the media title in UI.
     *
     * @param title The media title to set
     */
    private void setMediaTitle(String title) {
        mTitleView.setText(title);
    }

    /**
     * Loads thumbnail in UI.
     */
    private void loadThumbnail() {
        Glide.with(mContext)
                .load(Uri.fromFile(new File(mMediaItem.getData())))
                .apply(RequestOptions.centerCropTransform())
                .apply(RequestOptions.placeholderOf(R.drawable.cast_album_art_placeholder))
                .into((ImageView) mMiniControllerView.findViewById(
                        R.id.upnp_mini_controller_icon_view));
    }

    /**
     * Sets the state of the controller button.
     */
    private void setControllerButtonState() {
        if (mMediaItem instanceof ImageItem) {
            mMiniControllerButton.setContentDescription(
                    getResources().getString(R.string.controller_stop));
            mMiniControllerButton.setImageDrawable(
                    DrawableUtils.tintDrawable(
                            mContext,
                            R.drawable.ic_close_black_36dp,
                            R.color.upnp_mini_controller_button_color)
            );
        } else if (mMediaItem instanceof VideoItem) {
            mMiniControllerButton.setContentDescription(
                    getResources().getString(
                            MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState) ?
                                    R.string.controller_pause
                                    :
                                    R.string.controller_play)
            );
            mMiniControllerButton.setImageDrawable(
                    MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState) ?
                            DrawableUtils.tintDrawable(
                                    mContext,
                                    R.drawable.cast_ic_mini_controller_pause,
                                    R.color.upnp_mini_controller_button_color)
                            :
                            DrawableUtils.tintDrawable(
                                    mContext,
                                    R.drawable.cast_ic_mini_controller_play,
                                    R.color.upnp_mini_controller_button_color)
            );
        }
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter eventIntentFilter = new IntentFilter(
                UpnpPlayback.ACTION_AVTRANSPORT_SETAVTRANSPORTURI);
        eventIntentFilter.addAction(MediaBrowserActivity.ACTION_MEDIA_ITEM_SELECTED);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(
                mEventReceiver, eventIntentFilter);

        IntentFilter mediaEventIntentFilter = new IntentFilter(
                MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(
                mMediaEventReceiver, mediaEventIntentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
                mEventReceiver);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
                mMediaEventReceiver);
    }

    /**
     * Monitors events of a {@link UpnpSession} instance.
     */
    private class UpnpSessionManagerListenerImpl implements SessionManagerListener<UpnpSession> {
        @Override
        public void onSessionStarting(UpnpSession upnpSession) {
        }

        @Override
        public void onSessionStarted(UpnpSession upnpSession, String sessionId) {
            mUpnpSession = upnpSession;
            LogHelper.d(TAG, "onSessionStarted - mUpnpSession: " + mUpnpSession);
        }

        @Override
        public void onSessionStartFailed(UpnpSession upnpSession, int error) {
        }

        @Override
        public void onSessionEnding(UpnpSession upnpSession) {
        }

        @Override
        public void onSessionEnded(UpnpSession upnpSession, int error) {
            if (upnpSession == mUpnpSession) {
                mUpnpSession = null;
            }
        }

        @Override
        public void onSessionResuming(UpnpSession upnpSession, String sessionId) {
        }

        @Override
        public void onSessionResumed(UpnpSession upnpSession, boolean wasSuspended) {
            mUpnpSession = upnpSession;
            LogHelper.d(TAG, "onSessionResumed - mUpnpSession: " + mUpnpSession);
        }

        @Override
        public void onSessionResumeFailed(UpnpSession upnpSession, int error) {
        }

        @Override
        public void onSessionSuspended(UpnpSession upnpSession, int reason) {
        }
    }
}
