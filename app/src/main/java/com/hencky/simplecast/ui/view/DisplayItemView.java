package com.hencky.simplecast.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hencky.simplecast.R;
import com.hencky.simplecast.model.DisplayMode;
import com.hencky.simplecast.utils.DrawableUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.TextViewCompat;

public class DisplayItemView extends LinearLayout implements Checkable {

    private final Context mContext;
    private final int mTextViewPadding;
    private final int mTextViewWidth;
    private final int mIndicatorWidth;
    private final int mIndicatorHeight;
    private final String[] mDisplayModeStrings;
    private final TypedArray mDisplayModeDrawableIds;
    private Callback mCallback;
    private FrameLayout mIndicator;
    private int mDisplayModeIndex;
    private DisplayMode mDisplayMode = DisplayMode.LIST;
    private boolean mChecked;

    public DisplayItemView(@NonNull Context context) {
        this(context, null);
    }

    public DisplayItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DisplayItemView(@NonNull Context context, @Nullable AttributeSet attrs,
                           int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DisplayItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                           int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mContext = context.getApplicationContext();

        mTextViewPadding = getResources().getDimensionPixelSize(R.dimen.display_item_text_view_padding);
        mTextViewWidth = getResources().getDimensionPixelSize(R.dimen.display_item_text_view_width);
        mIndicatorWidth = getResources().getDimensionPixelSize(R.dimen.display_item_width);
        mIndicatorHeight = getResources().getDimensionPixelSize(R.dimen.display_item_indicator_height);
        mDisplayModeStrings = getResources().getStringArray(R.array.display_mode_entries);
        mDisplayModeDrawableIds = getResources().obtainTypedArray(R.array.display_mode_drawables);

        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        if (mChecked) {
            setTag(mDisplayMode.name());
            mIndicator.setVisibility(View.VISIBLE);
            if (mCallback != null) {
                mCallback.onDisplayModeSelected(mDisplayMode);
            }
        } else {
            mIndicator.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {

    }

    public void setDisplayMode(DisplayMode displayMode) {
        mDisplayMode = displayMode;
        mDisplayModeIndex = DisplayMode.valueOf(displayMode.name()).ordinal();
        createItemView();
    }

    private void createItemView() {
        removeAllViews();

        TextView displayItemTextView = new TextView(getContext());
        displayItemTextView.setText(mDisplayModeStrings[mDisplayModeIndex]);
        TextViewCompat.setTextAppearance(displayItemTextView, R.style.DisplayModeTextView);
        displayItemTextView.setGravity(Gravity.CENTER);
        displayItemTextView.setPadding(mTextViewPadding, mTextViewPadding, mTextViewPadding, 0);
        displayItemTextView.setLayoutParams(new LayoutParams(mTextViewWidth, mTextViewWidth));
        displayItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                DrawableUtils.tintDrawable(mContext,
                        mDisplayModeDrawableIds.getDrawable(mDisplayModeIndex),
                        R.color.display_item_icon_tint),
                null,
                null);
        displayItemTextView.setClickable(true);
        displayItemTextView.setBackgroundResource(R.drawable.button);
        displayItemTextView.setOnClickListener(v -> setChecked(true));
        addView(displayItemTextView);

        mIndicator = new FrameLayout(getContext());
        LayoutParams params = new LayoutParams(mIndicatorWidth, mIndicatorHeight);
        mIndicator.setLayoutParams(params);
        mIndicator.setBackground(new ColorDrawable(ContextCompat.getColor(getContext(),
                R.color.display_item_indicator)));
        mIndicator.setVisibility(View.INVISIBLE);
        addView(mIndicator);
    }

    /**
     * @param callback to be called
     */
    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    interface Callback {
        /**
         * On display mode selected.
         *
         * @param displayMode the selected display mode
         */
        void onDisplayModeSelected(DisplayMode displayMode);
    }
}
