package com.hencky.simplecast.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hencky.simplecast.R;
import com.hencky.simplecast.model.SortMode;
import com.hencky.simplecast.model.SortOrder;
import com.hencky.simplecast.model.SortType;
import com.hencky.simplecast.utils.DrawableUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.TextViewCompat;

public class SortItemView extends LinearLayout implements Checkable {

    private Context mContext;
    private Callback mCallback;
    private FrameLayout mIndicator;
    private ImageView mSortOrderIndicator;
    private int mSortModeIndex;
    private int mTextViewPadding;
    private int mTextViewWidth;
    private int mIndicatorWidth;
    private int mIndicatorHeight;
    private int mIndicatorMargingRight;
    private String[] mSortModeStrings;
    private TypedArray mSortModeDrawableIds;
    private SortType mSortType;
    private SortOrder mSortOrder = SortOrder.ASC;
    private boolean mChecked;

    private static final Map<String, List<Object>> SORT_ORDER_RESOURCES =
            new HashMap<>(2);

    static {
        List<Object> asc = new ArrayList<>(2);
        asc.add("arrow_drop_up");
        asc.add(R.drawable.ic_arrow_drop_up_black_10dp);
        SORT_ORDER_RESOURCES.put(SortOrder.ASC.name(), asc);

        List<Object> desc = new ArrayList<>(2);
        desc.add("arrow_drop_down");
        desc.add(R.drawable.ic_arrow_drop_down_black_10dp);
        SORT_ORDER_RESOURCES.put(SortOrder.DESC.name(), desc);
    }

    public SortItemView(@NonNull Context context) {
        this(context, null);
    }

    public SortItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SortItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public SortItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                        int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mContext = context;

        mTextViewPadding = getResources().getDimensionPixelSize(R.dimen.sort_item_text_view_padding);
        mTextViewWidth = getResources().getDimensionPixelSize(R.dimen.sort_item_text_view_width);
        mIndicatorWidth = getResources().getDimensionPixelSize(R.dimen.sort_item_width);
        mIndicatorHeight = getResources().getDimensionPixelSize(R.dimen.sort_item_indicator_height);
        mIndicatorMargingRight = getResources().getDimensionPixelSize(R.dimen.sort_item_marging_right);
        mSortModeStrings = getResources().getStringArray(R.array.sort_type_entries);
        mSortModeDrawableIds = getResources().obtainTypedArray(R.array.sort_mode_drawables);

        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        if (mChecked) {
            toggle();
            mIndicator.setVisibility(View.VISIBLE);
            mSortOrderIndicator.setVisibility(View.VISIBLE);
            if (mCallback != null) {
                mCallback.onSortModeSelected(new SortMode(mSortType, mSortOrder));
            }
        } else {
            reset();
            mIndicator.setVisibility(View.INVISIBLE);
            mSortOrderIndicator.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        toggleSortOrder();
    }

    private void toggleSortOrder() {
        if (mSortOrderIndicator.getVisibility() == View.VISIBLE) {
            if ("arrow_drop_up".equals(mSortOrderIndicator.getTag().toString())) {
                mSortOrder = SortOrder.DESC;
            } else {
                mSortOrder = SortOrder.ASC;
            }
            mSortOrderIndicator.setImageResource(getDrawableResId(mSortOrder));
            mSortOrderIndicator.setTag(getTag(mSortOrder));
        }
    }

    private void reset() {
        mSortOrder = SortOrder.ASC;
        mSortOrderIndicator.setImageResource(getDrawableResId(mSortOrder));
        mSortOrderIndicator.setTag(getTag(mSortOrder));
    }

    private String getTag(SortOrder sortOrder) {
        List<Object> list = SORT_ORDER_RESOURCES.get(sortOrder.name());
        return (list == null) ? null : (String) list.get(0);
    }

    private int getDrawableResId(SortOrder sortOrder) {
        List<Object> list = SORT_ORDER_RESOURCES.get(sortOrder.name());
        return (list == null) ? -1 : (int) list.get(1);
    }

    public void setSortMode(SortMode sortMode) {
        mSortType = sortMode.getSortType();
        mSortOrder = sortMode.getSortOrder();
        mSortModeIndex = SortType.valueOf(mSortType.name()).ordinal();
        createItemView();
    }

    private void createItemView() {
        removeAllViews();

        TextView sortItemTextView = new TextView(getContext());
        sortItemTextView.setText(mSortModeStrings[mSortModeIndex]);
        TextViewCompat.setTextAppearance(sortItemTextView, R.style.SortModeTextView);
        sortItemTextView.setGravity(Gravity.CENTER);
        sortItemTextView.setPadding(mTextViewPadding, mTextViewPadding, mTextViewPadding, 0);
        sortItemTextView.setLayoutParams(new LayoutParams(mTextViewWidth, mTextViewWidth));
        sortItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                DrawableUtils.tintDrawable(mContext,
                        mSortModeDrawableIds.getDrawable(mSortModeIndex),
                        R.color.sort_item_icon_tint),
                null,
                null);
        sortItemTextView.setClickable(true);
        sortItemTextView.setBackgroundResource(R.drawable.button);
        sortItemTextView.setOnClickListener(v -> setChecked(true));
        addView(sortItemTextView);

        LinearLayout indicatorLayout = new LinearLayout(getContext());
        indicatorLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));

        mIndicator = new FrameLayout(getContext());
        LayoutParams params = new LayoutParams(mIndicatorWidth, mIndicatorHeight);
        params.rightMargin = mIndicatorMargingRight;
        mIndicator.setLayoutParams(params);
        mIndicator.setBackground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color
                .sort_item_indicator)));
        mIndicator.setVisibility(View.INVISIBLE);
        indicatorLayout.addView(mIndicator);

        mSortOrderIndicator = new ImageView(getContext());
        mSortOrderIndicator.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
        mSortOrderIndicator.setImageResource(getDrawableResId(mSortOrder));
        mSortOrderIndicator.setTag(getTag(mSortOrder));
        mSortOrderIndicator.setImageTintList(ContextCompat.getColorStateList(getContext(),
                R.color.sort_item_indicator));
        mSortOrderIndicator.setVisibility(View.INVISIBLE);
        indicatorLayout.addView(mSortOrderIndicator);

        addView(indicatorLayout);
    }

    /**
     * @param callback to be called
     */
    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    interface Callback {
        /**
         * On sort mode selected.
         *
         * @param sortMode the selected sort mode
         */
        void onSortModeSelected(SortMode sortMode);
    }
}
