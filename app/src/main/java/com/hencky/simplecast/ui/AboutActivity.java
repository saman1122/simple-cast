package com.hencky.simplecast.ui;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import com.hencky.simplecast.R;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.PreferencesHelper;

import de.psdev.licensesdialog.LicensesDialog;

/**
 * An activity that presents a description of this application.
 */
public class AboutActivity extends AppCompatActivity {

    private static final String TAG = LogHelper.makeLogTag(AboutActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_about);

        setupActionBar();

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String fullAppName = getString(R.string.app_name) + " v" + packageInfo.versionName;
            TextView aboutTextView = findViewById(R.id.about_title);
            aboutTextView.setText(fullAppName);
        } catch (PackageManager.NameNotFoundException e) {
            LogHelper.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean nightModeEnabled = PreferencesHelper.getInstance(this).isNightModeEnabled();
        if (nightModeEnabled) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return false;
    }

    /**
     * Sets up the {@link androidx.appcompat.app.ActionBar}.
     */
    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Invoked callback when the libraries_desc TextView is clicked.
     */
    public void onClickLibraries(View view) {
        new LicensesDialog.Builder(AboutActivity.this)
                .setNotices(R.raw.notices)
                .setTitle(R.string.app_libraries)
                .build()
                .show();
    }
}
