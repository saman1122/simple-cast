package com.hencky.simplecast.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.hencky.simplecast.model.SortMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SortModeView extends LinearLayout implements SortItemView.Callback {

    private Callback mCallback;
    private SortMode mSortMode = new SortMode();

    public SortModeView(@NonNull Context context) {
        this(context, null);
    }

    public SortModeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SortModeView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public SortModeView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                        int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onSortModeSelected(SortMode sortMode) {
        // uncheck other children
        for (int i = 0; i < getChildCount(); i++) {
            SortItemView v = (SortItemView) getChildAt(i);
            String tag = (String) v.getTag();
            if (!sortMode.getSortType().name().equals(tag)) {
                v.setChecked(false);
            }
        }

        if (mCallback != null) {
            if (!mSortMode.equals(sortMode)) {
                mCallback.onSortModeSelected(sortMode);
                mSortMode = sortMode;
            }
        }
    }

    /**
     * Adds a View child to the container.
     *
     * @param sortMode The sort mode to add
     * @param checked  {@code true} if the View child is checked, otherwise {@code false}
     */
    public void addChild(SortMode sortMode, boolean checked) {
        if (checked) {
            mSortMode = sortMode;
        }
        SortItemView v = new SortItemView(getContext());
        v.setTag(sortMode.getSortType().name());
        v.setSortMode(sortMode);
        v.setChecked(checked);
        v.setCallback(this);
        addView(v, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f));
    }

    /**
     * Sets a callback to this class.
     *
     * @param callback to be called
     */
    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        /**
         * On sort mode selected.
         *
         * @param sortMode the selected sort mode
         */
        void onSortModeSelected(SortMode sortMode);
    }
}
