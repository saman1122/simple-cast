package com.hencky.simplecast.ui;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.mediarouter.app.MediaRouteActionProvider;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.ui.widget.MediaPlaybackControlView;
import com.hencky.simplecast.upnp.UpnpRouteProvider;
import com.hencky.simplecast.utils.DateTimeUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;
import com.hencky.simplecast.utils.PreferencesHelper;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;

/**
 * Represents a video controller for UPnP device.
 */
public class UpnpVideoControllerActivity extends AppCompatActivity
        implements MediaPlaybackControlView.Listener {

    private static final String TAG = LogHelper.makeLogTag(UpnpVideoControllerActivity.class);

    private UpnpRouteProvider mUpnpRouteProvider;
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter.Callback mMediaRouterCallback;
    private MediaPlaybackControlView mMediaPlaybackControlView;
    private FrameLayout mForeground;
    private DiscreteSeekBar mSeekbar;
    private TextView mElapsedTimeView;
    private TextView mRemainingTimeView;
    private ProgressBar mLoadingIndicator;
    private long mPlaybackTimeSeconds;
    private boolean mStartedTrackingTouch;
    private VideoItem mVideoItem;
    private Device mSelectedDevice;

    private boolean mMuted;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;

    /**
     * Broadcast receiver for tracking media events such as
     * changed media player state,
     * or obtained media player state,
     * or obtained mute state of the sound,
     * or obtained playback time.
     */
    private final BroadcastReceiver mMediaEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);

            if (intent == null)
                return;

            String action = intent.getAction();

            if (MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED.equals(action) ||
                    MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED.equals(action)) {
                mMediaPlayerState = (MediaPlayerState) intent.getSerializableExtra(
                        AppKeys.EXTRA_PLAYER_STATE);
                LogHelper.d(TAG, "Media player state: " + mMediaPlayerState);
                runOnUiThread(() -> {
                    if (MediaPlayerState.TRANSITIONING.equals(mMediaPlayerState) ||
                            MediaPlayerState.NONE.equals(mMediaPlayerState) ||
                            MediaPlayerState.UNKNOWN.equals(mMediaPlayerState)) {
                        mLoadingIndicator.setVisibility(View.VISIBLE);
                    } else if (MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
                        mSeekbar.setProgress(0);
                        mElapsedTimeView.setText(DateTimeUtils.toDurationString(0));
                        mLoadingIndicator.setVisibility(View.VISIBLE);
                    } else {
                        mLoadingIndicator.setVisibility(View.INVISIBLE);
                    }
                    setupPlaybackControlView();
                });

                if (MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
                    // Close this activity.
                    finish();
                }
            } else if (MediaPlaybackUtils.ACTION_MUTE_STATE_OBTAINED.equals(action)) {
                mMuted = intent.getBooleanExtra(AppKeys.EXTRA_MUTED, false);
                LogHelper.d(TAG, "Obtained mute state: " + mMuted);
                runOnUiThread(() -> mMediaPlaybackControlView.setMuteState(mMuted));
            } else if (MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED.equals(action)) {
                mPlaybackTimeSeconds = intent.getLongExtra(AppKeys.EXTRA_PLAYBACK_TIME, 0);
                LogHelper.d(TAG, "Obtained playback time: " + mPlaybackTimeSeconds);
                runOnUiThread(() -> {
                    if (!mStartedTrackingTouch) {
                        mSeekbar.setProgress((int) mPlaybackTimeSeconds);
                        mElapsedTimeView.setText(DateTimeUtils.toDurationString(
                                mPlaybackTimeSeconds * 1000));
                        mRemainingTimeView.setText(DateTimeUtils.toDurationString(
                                getRemainingTimeMs()));
                    }
                });
            }
        }
    };

    private final RequestListener<Drawable> mRequestListener = new RequestListener<Drawable>() {
        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                    Target<Drawable> target, boolean isFirstResource) {
            return false;
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                       DataSource dataSource, boolean isFirstResource) {
            mForeground.setForeground(ContextCompat.getDrawable(
                    UpnpVideoControllerActivity.this,
                    R.drawable.bg_gradient_light));
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSelectedDevice = getIntent().getParcelableExtra(AppKeys.EXTRA_DEVICE);
        mVideoItem = getIntent().getParcelableExtra(AppKeys.EXTRA_MEDIA_ITEM);

        // Get last configuration
        if (savedInstanceState != null) {
            mMediaPlayerState = (MediaPlayerState) savedInstanceState.getSerializable(
                    AppKeys.EXTRA_PLAYER_STATE);
            mPlaybackTimeSeconds = savedInstanceState.getLong(AppKeys.EXTRA_PLAYBACK_TIME);
            mMuted = savedInstanceState.getBoolean(AppKeys.EXTRA_MUTED, false);
        }

        setContentView(R.layout.activity_expanded_video_controller);

        setupToolbar();

        TextView mStatusView = findViewById(R.id.status_text);
        mMediaPlaybackControlView = findViewById(R.id.player_view);
        mElapsedTimeView = mMediaPlaybackControlView.findViewById(R.id.time);
        mRemainingTimeView = mMediaPlaybackControlView.findViewById(R.id.end_time);
        mSeekbar = findViewById(R.id.seekbar);
        mForeground = findViewById(R.id.foreground);
        mLoadingIndicator = findViewById(R.id.loading_indicator);

        loadBackdrop();

        setupSeekbar();

        setupMediaRouter();

        registerBroadcastReceivers();

        mStatusView.setText(String.format(getString(R.string.cast_casting_to_device), mSelectedDevice.getName()));

        mMediaPlaybackControlView.requestFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean nightModeEnabled = PreferencesHelper.getInstance(this).isNightModeEnabled();
        if (nightModeEnabled) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback);
        MediaPlaybackUtils.requestMediaPlayerState(UpnpVideoControllerActivity.this);
        MediaPlaybackUtils.requestMuteState(UpnpVideoControllerActivity.this);
    }

    @Override
    public void onPause() {
        mMediaRouter.removeCallback(mMediaRouterCallback);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(AppKeys.EXTRA_PLAYER_STATE, mMediaPlayerState);
        outState.putLong(AppKeys.EXTRA_PLAYBACK_TIME, mPlaybackTimeSeconds);
        outState.putBoolean(AppKeys.EXTRA_MUTED, mMuted);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.upnp_controller_menu, menu);
        MediaRouteActionProvider actionProvider =
                (MediaRouteActionProvider) MenuItemCompat.getActionProvider(menu
                        .findItem(R.id.media_route_menu_item));
        actionProvider.setRouteSelector(mMediaRouteSelector);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onPlayClicked() {
        MediaPlaybackUtils.startPlayback(this);
    }

    @Override
    public void onPauseClicked() {
        MediaPlaybackUtils.pausePlayback(this);
    }

    @Override
    public void onForwardClicked() {
        long newPlaybackPositionInSeconds = mPlaybackTimeSeconds +
                PreferencesHelper.getInstance(UpnpVideoControllerActivity.this)
                        .getForwardTimeInSeconds();
        MediaPlaybackUtils.seekToPlaybackPosition(this, newPlaybackPositionInSeconds);
    }

    @Override
    public void onReplayClicked() {
        long newPlaybackPositionInSeconds = mPlaybackTimeSeconds -
                PreferencesHelper.getInstance(UpnpVideoControllerActivity.this)
                        .getBackwardTimeInSeconds();
        MediaPlaybackUtils.seekToPlaybackPosition(this, newPlaybackPositionInSeconds);
    }

    @Override
    public void onStopClicked() {
        MediaPlaybackUtils.stopPlayback(this);
    }

    @Override
    public void onMuteToggled(boolean muted) {
        mMuted = muted;
        MediaPlaybackUtils.setMute(UpnpVideoControllerActivity.this, mMuted);
    }

    @Override
    public boolean isUpnpDeviceController() {
        return true;
    }

    /**
     * Sets up the Toolbar.
     */
    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_expand_more_white_36dp);
        getSupportActionBar().setTitle(mVideoItem.getTitle());
    }

    /**
     * Loads the backdrop.
     */
    private void loadBackdrop() {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_movie_black_104dp)
                .centerCrop()
                .dontAnimate();

        Glide.with(this)
                .load(Uri.fromFile(new File(mVideoItem.getData())))
                .apply(options)
                .listener(mRequestListener)
                .into((ImageView) findViewById(R.id.backdrop));
    }

    /**
     * Sets up the Seekbar.
     */
    private void setupSeekbar() {
        mSeekbar.setMax((int) (mVideoItem.getDuration() / 1000));
        mSeekbar.setProgress((int) mPlaybackTimeSeconds);
        mSeekbar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {

            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {
                mStartedTrackingTouch = true;
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                long progress = (long) seekBar.getProgress();
                if (mPlaybackTimeSeconds != progress) {
                    LogHelper.d(TAG, "playback time: " + mPlaybackTimeSeconds +
                            ", progress: " + progress);
                    MediaPlaybackUtils.seekToPlaybackPosition(
                            UpnpVideoControllerActivity.this,
                            progress);
                }
                mStartedTrackingTouch = false;
            }
        });

        mSeekbar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {

            @Override
            public int transform(int value) {
                return value;
            }

            @Override
            public String transformToString(int value) {
                return DateTimeUtils.toDurationString(value * 1000);
            }

            @Override
            public boolean useStringTransform() {
                return true;
            }
        });

        // Set times in Views
        mElapsedTimeView.setText(DateTimeUtils.toDurationString(mPlaybackTimeSeconds * 1000));
        mRemainingTimeView.setText(DateTimeUtils.toDurationString(getRemainingTimeMs()));
    }

    private Long getRemainingTimeMs() {
        return (mVideoItem.getDuration() - (mPlaybackTimeSeconds * 1000));
    }

    /**
     * Sets up the {@link MediaPlaybackControlView}.
     */
    private void setupPlaybackControlView() {
        if (MediaPlayerState.PLAYING.equals(mMediaPlayerState)) {
            mMediaPlaybackControlView.setPlaybackState(true);
        } else if (MediaPlayerState.SEEKED.equals(mMediaPlayerState)) {
            mMediaPlaybackControlView.setPlaybackState(true);
        } else {
            mMediaPlaybackControlView.setPlaybackState(false);
        }
        mMediaPlaybackControlView.setMuteState(mMuted);
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter mediaEventintentFilter =
                new IntentFilter(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED);
        mediaEventintentFilter.addAction(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED);
        mediaEventintentFilter.addAction(MediaPlaybackUtils.ACTION_MUTE_STATE_OBTAINED);
        mediaEventintentFilter.addAction(MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaEventReceiver,
                mediaEventintentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaEventReceiver);
    }

    /**
     * Sets up a {@link MediaRouter} for device discovery.
     */
    private void setupMediaRouter() {
        mMediaRouter = MediaRouter.getInstance(this);
        // Get a MediaRouteProvider to provide routes to UPnP devices
        mUpnpRouteProvider = UpnpRouteProvider.getInstance(this);
        // Create a MediaRouteSelector for the UPnP routes
        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(UpnpRouteProvider.CATEGORY_UPNP)
                .build();
        // Create a MediaRouter callback for discovery events
        mMediaRouterCallback = new UpnpVideoControllerActivity.MediaRouterCallback();
    }

    /**
     * Callback for receiving events about media routing changes.
     */
    private class MediaRouterCallback extends MediaRouter.Callback {

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo route, int reason) {
            LogHelper.d(TAG, "Unselected route: id=" + route.getId() + ", name=" +
                    route.getName() + ", reason=" + reason);
            if (MediaRouter.UNSELECT_REASON_STOPPED == reason) {
                // Case when the user pressed the stop casting button.
                if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
                    if (route.supportsControlCategory(UpnpRouteProvider.CATEGORY_UPNP)) {
                        Bundle extras = route.getExtras();
                        Device unselectedDevice = extras.getParcelable(AppKeys.EXTRA_DEVICE);
                        if (mSelectedDevice.getIdentifier().equals(
                                unselectedDevice.getIdentifier())) {
                            if (mUpnpRouteProvider.providesDevice(mSelectedDevice)) {
                                MediaPlaybackUtils.stopPlayback(
                                        UpnpVideoControllerActivity.this);
                            } else {
                                // Notify other application components (including this activity)
                                // about the change to STOPPED player state
                                // so they can update their state if needed.
                                MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                                        UpnpVideoControllerActivity.this,
                                        MediaPlayerState.STOPPED,
                                        DeviceType.UPNP);
                            }
                        }
                    }
                }
            }
            onRouteUnselected(router, route);
        }
    }
}
