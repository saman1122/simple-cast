package com.hencky.simplecast.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.playback.Playback;
import com.hencky.simplecast.ui.CastImageControllerActivity;
import com.hencky.simplecast.ui.CastVideoControllerActivity;
import com.hencky.simplecast.ui.MediaBrowserActivity;
import com.hencky.simplecast.utils.CastMediaUtils;
import com.hencky.simplecast.utils.DrawableUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

import java.io.File;

public class CastMiniControllerFragment extends Fragment {

    private static final String TAG = LogHelper.makeLogTag(CastMiniControllerFragment.class);

    public interface Listener {
        Device getSelectedDevice();
    }

    private Context mContext;
    private Listener mListener;
    private CastContext mCastContext;
    private CastSession mCastSession;
    private View mMiniControllerView;
    private TextView mTitleView;
    private ImageView mMiniControllerButton;
    private ProgressBar mLoadingIndicator;
    private ProgressBar mProgressBar;
    private MediaItem mMediaItem;
    private long mPlaybackTimeMs = 0;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;

    private final SessionManagerListener<CastSession> mCastSessionManagerListener =
            new CastSessionManagerListenerImpl();

    /**
     * Broadcast receiver for tracking various events.
     */
    private final BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (MediaBrowserActivity.ACTION_MEDIA_ITEM_SELECTED.equals(action)) {
                    mLoadingIndicator.setVisibility(View.VISIBLE);
                    mMediaItem = intent.getParcelableExtra(AppKeys.EXTRA_MEDIA_ITEM);
                    registerRemoteMediaClientConf();
                } else if (Playback.ACTION_SETUP.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(AppKeys
                                    .EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received Setup action invocation result: " +
                            result.name());
                    if (ActionInvocationResult.SUCCESS.equals(result)) {
                        setupMiniController();
                        setControllerButtonState();
                        mMiniControllerView.setVisibility(View.VISIBLE);
                        mMiniControllerButton.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    };

    /**
     * Broadcast receiver for tracking media events such as
     * changed media player state.
     */
    private final BroadcastReceiver mMediaEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);

            if (intent == null)
                return;

            String action = intent.getAction();

            if (MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED.equals(action) ||
                    MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED.equals(action)) {
                mMediaPlayerState = (MediaPlayerState) intent.getSerializableExtra(
                        AppKeys.EXTRA_PLAYER_STATE);
                LogHelper.d(TAG, "Media player state: " + mMediaPlayerState);
                DeviceType deviceType = (DeviceType) intent.getSerializableExtra(
                        AppKeys.EXTRA_DEVICE_TYPE);
                if (!DeviceType.CAST.equals(deviceType)) {
                    LogHelper.d(TAG, "Ignoring media player state for device type " +
                            deviceType.name());
                    return;
                }
                mMiniControllerView.setVisibility(View.VISIBLE);
                if (MediaPlayerState.PAUSED.equals(mMediaPlayerState) ||
                        MediaPlayerState.PLAYING.equals(mMediaPlayerState)) {
                    setControllerButtonState();
                    mLoadingIndicator.setVisibility(View.INVISIBLE);
                    mMiniControllerButton.setVisibility(View.VISIBLE);
                } else {
                    mLoadingIndicator.setVisibility(View.VISIBLE);
                    mMiniControllerButton.setVisibility(View.INVISIBLE);
                    if (MediaPlayerState.STOPPED.equals(mMediaPlayerState) ||
                            MediaPlayerState.UNKNOWN.equals(mMediaPlayerState)) {
                        // Hide the mini controller view.
                        mMiniControllerView.setVisibility(View.GONE);
                    }
                }
            }
        }
    };

    /**
     * Callback for tracking player status changes.
     */
    private final RemoteMediaClient.Callback mRemoteMediaClientCallback =
            new RemoteMediaClient.Callback() {
                @Override
                public void onStatusUpdated() {
                    int playerState = CastMediaUtils.getMediaPlayerState(mContext,
                            mCastSession);
                    MediaPlayerState mediaPlayerState = CastMediaUtils.toMediaPlayerState(
                            mContext, playerState);
                    if (mMediaPlayerState.equals(mediaPlayerState)) {
                        LogHelper.d(TAG,
                                "onStatusUpdated - ignoring identical media player state: "
                                        + mediaPlayerState);
                        return;
                    }
                    mMediaPlayerState = mediaPlayerState;
                    LogHelper.d(TAG, "onStatusUpdated - media player state: " +
                            mMediaPlayerState);

                    MediaInfo mediaInfo = CastMediaUtils.getMediaInfo(mContext, mCastSession);
                    if (mediaInfo != null) {
                        MediaMetadata mediaMetadata = mediaInfo.getMetadata();
                        String mediaTitle = mediaMetadata.getString(MediaMetadata.KEY_TITLE);
                        if (!mMediaItem.getTitle().equals(mediaTitle)) {
                            LogHelper.d(TAG, "onStatusUpdated - " +
                                    "ignoring change of media player state for media " +
                                    mediaTitle);
                            return;
                        }
                    }

                    // Notify other application components
                    // about the changed player state
                    // so they can update their state if needed.
                    MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                            mContext, mMediaPlayerState, DeviceType.CAST);
                }
            };

    /**
     * Listener that gets updates on the progress of the currently playing media.
     */
    private final RemoteMediaClient.ProgressListener mRemoteMediaClientProgressListener =
            (progressMs, durationMs) -> {
                mPlaybackTimeMs = progressMs;
                mProgressBar.setProgress((int) mPlaybackTimeMs / 1000);
            };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        try {
            mListener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement Listener interface.");
        }
    }

    @Override
    public void onDetach() {
        mContext = null;
        mListener = null;
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        registerBroadcastReceivers();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mMiniControllerView = inflater.inflate(R.layout.fragment_cast_mini_controller,
                container, false);
        mMiniControllerView.setVisibility(View.GONE);
        mMiniControllerView.setOnClickListener(view -> {
            if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
                if (mMediaItem instanceof ImageItem) {
                    Intent intent = new Intent(mContext, CastImageControllerActivity.class);
                    intent.putExtra(AppKeys.EXTRA_DEVICE, mListener.getSelectedDevice());
                    intent.putExtra(AppKeys.EXTRA_MEDIA_ITEM, mMediaItem);
                    intent.putExtra(AppKeys.EXTRA_PLAYER_STATE, mMediaPlayerState);
                    startActivity(intent);
                } else if (mMediaItem instanceof VideoItem) {
                    Intent intent = new Intent(mContext, CastVideoControllerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(AppKeys.EXTRA_DEVICE, mListener.getSelectedDevice());
                    intent.putExtra(AppKeys.EXTRA_MEDIA_ITEM, mMediaItem);
                    intent.putExtra(AppKeys.EXTRA_PLAYER_STATE, mMediaPlayerState);
                    startActivity(intent);
                }
            }
        });

        mTitleView = mMiniControllerView.findViewById(R.id.cast_mini_controller_title_view);

        mLoadingIndicator = mMiniControllerView.findViewById(
                R.id.cast_mini_controller_loading_indicator);

        mProgressBar = mMiniControllerView.findViewById(R.id.cast_mini_controller_progress_bar);
        mProgressBar.setProgress((int) mPlaybackTimeMs);

        mMiniControllerButton = mMiniControllerView.findViewById(
                R.id.cast_mini_controller_button);
        mMiniControllerButton.setOnClickListener(view -> {
                    if (mCastSession != null) {
                        final RemoteMediaClient remoteMediaClient =
                                mCastSession.getRemoteMediaClient();
                        if (remoteMediaClient != null) {
                            if (mMediaItem instanceof ImageItem) {
                                MediaPlaybackUtils.stopPlayback(mContext);
                            } else if (mMediaItem instanceof VideoItem) {
                                if (remoteMediaClient.isPlaying()) {
                                    MediaPlaybackUtils.pausePlayback(mContext);
                                } else {
                                    MediaPlaybackUtils.startPlayback(mContext);
                                }
                            }
                        }
                    }
                }
        );

        return mMiniControllerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCastContext = CastContext.getSharedInstance(mContext);

        if (mCastSession != null && MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
            setupMiniController();
            setControllerButtonState();
            mLoadingIndicator.setVisibility(View.INVISIBLE);
            mMiniControllerButton.setVisibility(View.VISIBLE);
            mMiniControllerView.setVisibility(View.VISIBLE);
        } else {
            mMiniControllerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mCastContext.getSessionManager().addSessionManagerListener(mCastSessionManagerListener,
                CastSession.class);
        if (mCastSession != null) {
            registerRemoteMediaClientConf();
            LogHelper.d(TAG, "Requesting media player state");
            MediaPlaybackUtils.requestMediaPlayerState(mContext);
        }
    }

    @Override
    public void onPause() {
        mCastContext.getSessionManager().removeSessionManagerListener(
                mCastSessionManagerListener, CastSession.class);
        unregisterRemoteMediaClientConf(true);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterRemoteMediaClientConf(false);
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter eventIntentFilter = new IntentFilter(Playback.ACTION_SETUP);
        eventIntentFilter.addAction(MediaBrowserActivity.ACTION_MEDIA_ITEM_SELECTED);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(
                mEventReceiver, eventIntentFilter);

        IntentFilter mediaEventIntentFilter =
                new IntentFilter(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(
                mMediaEventReceiver, mediaEventIntentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
                mEventReceiver);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
                mMediaEventReceiver);
    }

    /**
     * Sets up the mini controller.
     */
    private void setupMiniController() {
        if (mMediaItem == null)
            return;
        if (mMediaItem instanceof VideoItem) {
            VideoItem videoItem = (VideoItem) mMediaItem;
            mProgressBar.setMax((int) (videoItem.getDuration() / 1000));
        }
        setMediaTitle(mMediaItem.getTitle());
        loadThumbnail();
    }

    /**
     * Sets the state of the controller button.
     */
    private void setControllerButtonState() {
        if (mMediaItem instanceof ImageItem) {
            mMiniControllerButton.setContentDescription(getResources().getString(
                    R.string.controller_stop));
            mMiniControllerButton.setImageDrawable(DrawableUtils.tintDrawable(
                    mContext,
                    R.drawable.ic_close_black_36dp,
                    R.color.cast_mini_controller_button_color)
            );
        } else if (mMediaItem instanceof VideoItem) {
            if (mCastSession != null) {
                final RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
                if (remoteMediaClient != null) {
                    mMiniControllerButton.setContentDescription(
                            getResources().getString(remoteMediaClient.isPlaying() ?
                                    R.string.controller_pause : R.string.controller_play)
                    );
                    mMiniControllerButton.setImageDrawable(remoteMediaClient.isPlaying() ?
                            DrawableUtils.tintDrawable(
                                    mContext,
                                    R.drawable.cast_ic_mini_controller_pause,
                                    R.color.cast_mini_controller_button_color)
                            :
                            DrawableUtils.tintDrawable(
                                    mContext,
                                    R.drawable.cast_ic_mini_controller_play,
                                    R.color.cast_mini_controller_button_color)
                    );
                }
            }
        }
    }

    /**
     * Sets the media title in UI.
     *
     * @param title The media title to set
     */
    private void setMediaTitle(String title) {
        mTitleView.setText(title);
    }

    /**
     * Loads thumbnail in UI.
     */
    private void loadThumbnail() {
        Glide.with(mContext)
                .load(Uri.fromFile(new File(mMediaItem.getData())))
                .apply(RequestOptions.centerCropTransform())
                .apply(RequestOptions.placeholderOf(R.drawable.cast_album_art_placeholder))
                .into((ImageView) mMiniControllerView.findViewById(
                        R.id.cast_mini_controller_icon_view));
    }

    /**
     * Registers the configuration of the {@link RemoteMediaClient} if needed.
     */
    private void registerRemoteMediaClientConf() {
        if (mMediaItem != null) {
            if (mMediaItem instanceof ImageItem) {
                CastMediaUtils.registerRemoteMediaClientConf(mContext,
                        mCastSession,
                        mRemoteMediaClientCallback,
                        null);
            } else if (mMediaItem instanceof VideoItem) {
                CastMediaUtils.registerRemoteMediaClientConf(mContext,
                        mCastSession,
                        mRemoteMediaClientCallback,
                        mRemoteMediaClientProgressListener);
            }
        }
    }

    /**
     * Unregisters the configuration of the {@link RemoteMediaClient} if needed.
     *
     * @param onPause {@code true} when this activity is about to be on pause,
     *                meaning that only the progress listener has to be unregistered,
     *                otherwise {@code false}.
     */
    private void unregisterRemoteMediaClientConf(boolean onPause) {
        if (mMediaItem != null) {
            if (mMediaItem instanceof ImageItem) {
                CastMediaUtils.unregisterRemoteMediaClientConf(mContext,
                        mCastSession,
                        onPause ? null : mRemoteMediaClientCallback,
                        null);
            } else if (mMediaItem instanceof VideoItem) {
                CastMediaUtils.unregisterRemoteMediaClientConf(mContext,
                        mCastSession,
                        onPause ? null : mRemoteMediaClientCallback,
                        mRemoteMediaClientProgressListener);
            }
        }
    }

    /**
     * Monitors events of a {@link CastSession} instance.
     */
    private class CastSessionManagerListenerImpl implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionStarting(CastSession castSession) {
        }

        @Override
        public void onSessionStarted(CastSession castSession, String s) {
            mCastSession = castSession;
        }

        @Override
        public void onSessionStartFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionEnding(CastSession castSession) {
        }

        @Override
        public void onSessionEnded(CastSession castSession, int i) {
            if (castSession == mCastSession) {
                mCastSession = null;
            }
        }

        @Override
        public void onSessionResuming(CastSession castSession, String s) {
        }

        @Override
        public void onSessionResumed(CastSession castSession, boolean b) {
            mCastSession = castSession;
        }

        @Override
        public void onSessionResumeFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionSuspended(CastSession castSession, int i) {
        }
    }
}
