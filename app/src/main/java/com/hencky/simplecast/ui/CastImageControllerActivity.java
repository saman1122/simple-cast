package com.hencky.simplecast.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.mediarouter.app.MediaRouteActionProvider;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.utils.CastMediaUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;
import com.hencky.simplecast.utils.PreferencesHelper;

import java.io.File;

public class CastImageControllerActivity extends AppCompatActivity {

    private static final String TAG = LogHelper.makeLogTag(CastImageControllerActivity.class);

    private CastContext mCastContext;
    private CastSession mCastSession;
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter.Callback mMediaRouterCallback;
    private FrameLayout mForeground;
    private ProgressBar mLoadingIndicator;
    private MediaItem mMediaItem;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;

    private final SessionManagerListener<CastSession> mCastSessionManagerListener =
            new CastSessionManagerListenerImpl();

    /**
     * Monitors the status of a request while backdrop image loads.
     */
    private final RequestListener<Drawable> mRequestListener = new RequestListener<Drawable>() {
        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                    Target<Drawable> target, boolean isFirstResource) {
            return false;
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                       DataSource dataSource, boolean isFirstResource) {
            mForeground.setForeground(ContextCompat.getDrawable(
                    CastImageControllerActivity.this, R.drawable.bg_gradient_light));
            mLoadingIndicator.setVisibility(View.INVISIBLE);
            return false;
        }
    };

    /**
     * Broadcast receiver for tracking media events such as
     * changed media player state.
     */
    private final BroadcastReceiver mMediaEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);

            if (intent == null)
                return;

            String action = intent.getAction();

            if (MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED.equals(action) ||
                    MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED.equals(action)) {
                mMediaPlayerState = (MediaPlayerState) intent.getSerializableExtra(
                        AppKeys.EXTRA_PLAYER_STATE);
                LogHelper.d(TAG, "Media player state: " + mMediaPlayerState);

                runOnUiThread(() -> {
                    if (MediaPlayerState.TRANSITIONING.equals(mMediaPlayerState) ||
                            MediaPlayerState.NONE.equals(mMediaPlayerState) ||
                            MediaPlayerState.UNKNOWN.equals(mMediaPlayerState)) {
                        mLoadingIndicator.setVisibility(View.VISIBLE);
                    } else if (MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
                        // Close this activity.
                        finish();
                    } else {
                        mLoadingIndicator.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }
    };

    /**
     * Callback for tracking player status changes.
     */
    private final RemoteMediaClient.Callback mRemoteMediaClientCallback =
            new RemoteMediaClient.Callback() {
                @Override
                public void onStatusUpdated() {
                    final int playerState = CastMediaUtils.getMediaPlayerState(
                            CastImageControllerActivity.this, mCastSession);
                    MediaPlayerState mediaPlayerState = CastMediaUtils.toMediaPlayerState(
                            CastImageControllerActivity.this, playerState);
                    if (mMediaPlayerState.equals(mediaPlayerState)) {
                        LogHelper.d(TAG,
                                "onStatusUpdated - ignoring identical media player state: "
                                        + mediaPlayerState);
                        return;
                    }
                    mMediaPlayerState = mediaPlayerState;
                    LogHelper.d(TAG, "onStatusUpdated - media player state: " +
                            mMediaPlayerState);
                    if (MediaPlayerState.PAUSED.equals(mMediaPlayerState)) {
                        mLoadingIndicator.setVisibility(View.INVISIBLE);
                    } else {
                        mLoadingIndicator.setVisibility(View.VISIBLE);
                        if (MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
                            // Notify other application components (including this activity)
                            // about the change to STOPPED player state
                            // so they can update their state if needed.
                            MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                                    CastImageControllerActivity.this,
                                    MediaPlayerState.STOPPED,
                                    DeviceType.CAST);
                        }
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCastContext = CastContext.getSharedInstance(CastImageControllerActivity.this);

        Device device = getIntent().getParcelableExtra(AppKeys.EXTRA_DEVICE);
        mMediaItem = getIntent().getParcelableExtra(AppKeys.EXTRA_MEDIA_ITEM);

        // Get last configuration
        if (savedInstanceState != null) {
            mMediaPlayerState = (MediaPlayerState) savedInstanceState.getSerializable(
                    AppKeys.EXTRA_PLAYER_STATE);
        }

        setContentView(R.layout.activity_expanded_image_controller);

        setupToolbar();

        setupMediaRouter();

        TextView mStatusView = findViewById(R.id.status_text);
        mForeground = findViewById(R.id.foreground);
        mLoadingIndicator = findViewById(R.id.loading_indicator);

        ImageButton stopButton = findViewById(R.id.btn_stop);
        stopButton.setOnClickListener(v ->
                MediaPlaybackUtils.stopPlayback(CastImageControllerActivity.this)
        );

        loadBackdrop();


        mStatusView.setText(String.format(getString(R.string.cast_casting_to_device),
                device.getName()));

        registerBroadcastReceivers();
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean nightModeEnabled = PreferencesHelper.getInstance(this).isNightModeEnabled();
        if (nightModeEnabled) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        mCastContext.getSessionManager().addSessionManagerListener(
                mCastSessionManagerListener, CastSession.class);
        registerRemoteMediaClientConf();
        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback);
        MediaPlaybackUtils.requestMediaPlayerState(CastImageControllerActivity.this);
    }

    @Override
    public void onPause() {
        mCastContext.getSessionManager().removeSessionManagerListener(
                mCastSessionManagerListener, CastSession.class);
        unregisterRemoteMediaClientConf(true);
        mMediaRouter.removeCallback(mMediaRouterCallback);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterRemoteMediaClientConf(false);
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(AppKeys.EXTRA_PLAYER_STATE, mMediaPlayerState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.cast_controller_menu, menu);
        MediaRouteActionProvider actionProvider =
                (MediaRouteActionProvider) MenuItemCompat.getActionProvider(menu
                        .findItem(R.id.media_route_menu_item));
        actionProvider.setRouteSelector(mMediaRouteSelector);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    /**
     * Sets up the Toolbar.
     */
    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_expand_more_white_36dp);
        getSupportActionBar().setTitle(mMediaItem.getTitle());
    }

    /**
     * Loads the backdrop.
     */
    private void loadBackdrop() {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_image_black_104dp)
                .centerCrop()
                .dontAnimate();

        Glide.with(this)
                .load(Uri.fromFile(new File(mMediaItem.getData())))
                .apply(options)
                .listener(mRequestListener)
                .into((ImageView) findViewById(R.id.backdrop));
    }

    /**
     * Sets up a {@link MediaRouter} for device discovery.
     */
    private void setupMediaRouter() {
        mMediaRouter = MediaRouter.getInstance(this);
        // Create a MediaRouteSelector for the Cast routes
        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(CastMediaControlIntent.categoryForCast(getResources()
                        .getString(R.string.cast_app_id)))
                .build();
        // Create a MediaRouter callback for discovery events
        mMediaRouterCallback = new CastImageControllerActivity.MediaRouterCallback();
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter mediaEventIntentFilter = new IntentFilter(
                MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaEventReceiver,
                mediaEventIntentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaEventReceiver);
    }

    /**
     * Registers the configuration of the {@link RemoteMediaClient} if needed.
     */
    private void registerRemoteMediaClientConf() {
        CastMediaUtils.registerRemoteMediaClientConf(
                CastImageControllerActivity.this,
                mCastSession,
                mRemoteMediaClientCallback,
                null);
    }

    /**
     * Unregisters the configuration of the {@link RemoteMediaClient} if needed.
     *
     * @param onPause {@code true} when this activity is about to be on pause,
     *                meaning that only the progress listener has to be unregistered,
     *                otherwise {@code false}.
     */
    private void unregisterRemoteMediaClientConf(boolean onPause) {
        CastMediaUtils.unregisterRemoteMediaClientConf(
                CastImageControllerActivity.this,
                mCastSession,
                onPause ? null : mRemoteMediaClientCallback,
                null);
    }

    /**
     * Callback for receiving events about media routing changes.
     */
    private class MediaRouterCallback extends MediaRouter.Callback {

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo route, int reason) {
            LogHelper.d(TAG, "Unselected route: id=" + route.getId() + ", name=" +
                    route.getName() + ", reason=" + reason);

            if (MediaRouter.UNSELECT_REASON_STOPPED == reason) {
                // Case when the user pressed the stop casting button.
                if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
                    // Notify other application components (including this activity)
                    // about the change to STOPPED player state
                    // so they can update their state if needed.
                    MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                            CastImageControllerActivity.this,
                            MediaPlayerState.STOPPED,
                            DeviceType.CAST);
                }
            }

            onRouteUnselected(router, route);
        }
    }

    /**
     * Monitors events of a {@link CastSession} instance.
     */
    private class CastSessionManagerListenerImpl implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionStarting(CastSession castSession) {
        }

        @Override
        public void onSessionStarted(CastSession castSession, String s) {
            mCastSession = castSession;
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionStartFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionEnding(CastSession castSession) {
        }

        @Override
        public void onSessionEnded(CastSession castSession, int i) {
            if (castSession == mCastSession) {
                mCastSession = null;
            }
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResuming(CastSession castSession, String s) {
        }

        @Override
        public void onSessionResumed(CastSession castSession, boolean b) {
            mCastSession = castSession;
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResumeFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionSuspended(CastSession castSession, int i) {
        }
    }
}
