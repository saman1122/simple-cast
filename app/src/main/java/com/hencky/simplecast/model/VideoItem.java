package com.hencky.simplecast.model;

import android.os.Parcel;

/**
 * Represents a Video item.
 */
public class VideoItem extends MediaItem {

    /**
     * The duration of the video file, in milliseconds.
     */
    private Long duration;

    /**
     * The resolution of the video.
     */
    private String resolution;

    /**
     * The video mini thumbnail.
     */
    private ImageItem miniThumb;

    /**
     * The video full thumbnail.
     */
    private ImageItem fullThumb;

    public VideoItem() {
    }

    private VideoItem(Parcel in) {
        super(in);
        this.duration = (Long) in.readValue(Long.class.getClassLoader());
        this.resolution = in.readString();
        this.miniThumb = in.readParcelable(ImageItem.class.getClassLoader());
        this.fullThumb = in.readParcelable(ImageItem.class.getClassLoader());
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public ImageItem getMiniThumb() {
        return miniThumb;
    }

    public void setMiniThumb(ImageItem miniThumb) {
        this.miniThumb = miniThumb;
    }

    public ImageItem getFullThumb() {
        return fullThumb;
    }

    public void setFullThumb(ImageItem fullThumb) {
        this.fullThumb = fullThumb;
    }

    @Override
    public String toString() {
        return "VideoItem{" +
                "duration=" + duration +
                ", resolution='" + resolution + '\'' +
                ", miniThumb='" + miniThumb + '\'' +
                ", fullThumb='" + fullThumb + '\'' +
                ", " + super.toString() +
                "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.duration);
        dest.writeString(this.resolution);
        dest.writeParcelable(this.miniThumb, flags);
        dest.writeParcelable(this.fullThumb, flags);
    }

    public static final Creator<VideoItem> CREATOR = new Creator<VideoItem>() {
        @Override
        public VideoItem createFromParcel(Parcel source) {
            return new VideoItem(source);
        }

        @Override
        public VideoItem[] newArray(int size) {
            return new VideoItem[size];
        }
    };
}
