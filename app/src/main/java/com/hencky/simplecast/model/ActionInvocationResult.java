package com.hencky.simplecast.model;

public enum ActionInvocationResult {
    FAILURE, SUCCESS
}
