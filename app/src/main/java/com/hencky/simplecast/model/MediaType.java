package com.hencky.simplecast.model;

/**
 * Represents a media type.
 */
public enum MediaType {
    IMAGE, VIDEO
}
