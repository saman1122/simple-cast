package com.hencky.simplecast.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class SortMode implements Parcelable {
    private final SortType sortType;
    private final SortOrder sortOrder;

    public SortMode() {
        this(SortType.NAME, SortOrder.ASC);
    }

    public SortMode(SortType sortType, SortOrder sortOrder) {
        this.sortType = sortType;
        this.sortOrder = sortOrder;
    }

    public SortType getSortType() {
        return sortType;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    @Override
    public String toString() {
        return "SortMode{" +
                "sortType=" + sortType +
                ", sortOrder=" + sortOrder +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sortType == null ? -1 : this.sortType.ordinal());
        dest.writeInt(this.sortOrder == null ? -1 : this.sortOrder.ordinal());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortMode sortMode = (SortMode) o;
        return sortType == sortMode.sortType &&
                sortOrder == sortMode.sortOrder;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sortType, sortOrder);
    }

    private SortMode(Parcel in) {
        int tmpSortType = in.readInt();
        this.sortType = tmpSortType == -1 ? null : SortType.values()[tmpSortType];
        int tmpSortOrder = in.readInt();
        this.sortOrder = tmpSortOrder == -1 ? null : SortOrder.values()[tmpSortOrder];
    }

    public static final Parcelable.Creator<SortMode> CREATOR = new Parcelable.Creator<SortMode>() {
        @Override
        public SortMode createFromParcel(Parcel source) {
            return new SortMode(source);
        }

        @Override
        public SortMode[] newArray(int size) {
            return new SortMode[size];
        }
    };
}
