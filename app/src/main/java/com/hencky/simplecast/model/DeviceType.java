package com.hencky.simplecast.model;

public enum DeviceType {
    CAST, UPNP
}
