package com.hencky.simplecast.model;

public enum SortOrder {
    ASC, DESC
}
