package com.hencky.simplecast.model;

import android.os.Parcel;

/**
 * Represents an Image item.
 */
public class ImageItem extends MediaItem {

    public ImageItem() {
    }

    private ImageItem(Parcel in) {
        super(in);
    }

    @Override
    public String toString() {
        return "ImageItem{" + super.toString() + "}";
    }

    public static final Creator<ImageItem> CREATOR = new Creator<ImageItem>() {
        @Override
        public ImageItem createFromParcel(Parcel source) {
            return new ImageItem(source);
        }

        @Override
        public ImageItem[] newArray(int size) {
            return new ImageItem[size];
        }
    };
}
