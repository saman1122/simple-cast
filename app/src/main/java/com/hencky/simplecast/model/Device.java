package com.hencky.simplecast.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.hencky.simplecast.AppKeys;

import java.util.Objects;

/**
 * Represents a device.
 */
public class Device implements Parcelable {
    private String identifier;
    private String name;
    private DeviceType deviceType;

    public Device() {
    }

    public Device(String identifier, String name, DeviceType deviceType) {
        this.identifier = identifier;
        this.name = name;
        this.deviceType = deviceType;
    }

    protected Device(Parcel in) {
        this.identifier = in.readString();
        this.name = in.readString();
        int tmpDeviceType = in.readInt();
        this.deviceType = tmpDeviceType == -1 ? null : DeviceType.values()[tmpDeviceType];
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public static Device getFromBundle(Bundle bundle) {
        if (bundle == null) {
            return null;
        } else {
            bundle.setClassLoader(Device.class.getClassLoader());
            return (Device) bundle.getParcelable(AppKeys.EXTRA_DEVICE);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.identifier);
        dest.writeString(this.name);
        dest.writeInt(this.deviceType == null ? -1 : this.deviceType.ordinal());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return Objects.equals(identifier, device.identifier) &&
                Objects.equals(name, device.name) &&
                deviceType == device.deviceType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(identifier, name, deviceType);
    }

    @Override
    public String toString() {
        return "Device{" +
                "identifier='" + identifier + '\'' +
                ", name='" + name + '\'' +
                ", deviceType=" + deviceType +
                '}';
    }

    public static final Parcelable.Creator<Device> CREATOR = new Parcelable.Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel source) {
            return new Device(source);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };
}
