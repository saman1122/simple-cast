package com.hencky.simplecast.cache;

import android.content.Context;

import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.ExternalPreferredCacheDiskCacheFactory;
import com.bumptech.glide.signature.ObjectKey;
import com.hencky.simplecast.utils.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Cache implementation that caches files directly onto the hard disk in the specified directory.
 * The default disk usage size is 20MB, but is configurable.
 * This implementation uses DiskCache object from Glide library.
 */
public class DiskBasedCache {

    /**
     * Default maximum disk usage in bytes.
     */
    private static final int DEFAULT_DISK_USAGE_BYTES = 1024 * 1024 * 20; // 20MB

    /**
     * The interface that interacts with the disk cache.
     */
    private final DiskCache mDiskCache;

    /**
     * Constructs an instance of the DiskBasedCache for the given disk cache name using the default
     * maximum cache size of 20MB.
     *
     * @param context       The context to use.
     * @param diskCacheName The disk cache name.
     */
    public DiskBasedCache(Context context, String diskCacheName) {
        this(context, diskCacheName, DEFAULT_DISK_USAGE_BYTES);
    }

    /**
     * Constructs an instance of the DiskBasedCache for the given disk cache name using the given
     * maximum cache size of 5MB.
     *
     * @param context             The context to use.
     * @param diskCacheName       The disk cache name.
     * @param maxCacheSizeInBytes The maximum size of the cache in bytes.
     */
    public DiskBasedCache(Context context, String diskCacheName, int maxCacheSizeInBytes) {
        ExternalPreferredCacheDiskCacheFactory diskCacheFactory =
                new ExternalPreferredCacheDiskCacheFactory(context, diskCacheName,
                        maxCacheSizeInBytes);
        mDiskCache = diskCacheFactory.build();
    }

    /**
     * Gets file from disk cache for the given key.
     *
     * @param key The key
     * @return the file from cache
     */
    public File getFileFromCache(String key) {
        return mDiskCache.get(new ObjectKey(key));
    }


    /**
     * Puts data in the disk cache for a given key.
     *
     * @param key  The key for data
     * @param data The data to put in the cache
     */
    public void putDataInCache(String key, byte[] data) {
        mDiskCache.put(new ObjectKey(key), file -> {
            try {
                FileUtils.writeByteArrayToFile(file, data);
            } catch (IOException e) {
                e.getStackTrace();
            }
            return true;
        });
    }
}
