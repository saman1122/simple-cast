package com.hencky.simplecast.cast;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.OptionsProvider;
import com.google.android.gms.cast.framework.SessionProvider;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.ImagePicker;
import com.google.android.gms.common.images.WebImage;
import com.hencky.simplecast.R;
import com.hencky.simplecast.upnp.UpnpSessionProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements {@link OptionsProvider} to create and initialize a CastContext.
 */
public class CastOptionsProvider implements OptionsProvider {

    @Override
    public CastOptions getCastOptions(Context context) {

        CastMediaOptions mediaOptions = new CastMediaOptions.Builder()
                .setImagePicker(new ImagePickerImpl())
                .build();

        return new CastOptions.Builder()
                .setReceiverApplicationId(context.getString(R.string.cast_app_id))
                .setCastMediaOptions(mediaOptions)
                .build();
    }

    @Override
    public List<SessionProvider> getAdditionalSessionProviders(Context context) {
        List<SessionProvider> sessionProviders = new ArrayList<>();
        sessionProviders.add(new UpnpSessionProvider(context));
        return sessionProviders;
    }

    private static class ImagePickerImpl extends ImagePicker {

        @Override
        public WebImage onPickImage(MediaMetadata mediaMetadata, @NonNull ImageHints hints) {
            if ((mediaMetadata == null) || !mediaMetadata.hasImages()) {
                return null;
            }
            List<WebImage> images = mediaMetadata.getImages();
            if (images.size() == 1) {
                return images.get(0);
            } else {
                switch (hints.getType()) {
                    case ImagePicker.IMAGE_TYPE_MEDIA_ROUTE_CONTROLLER_DIALOG_BACKGROUND:
                    case ImagePicker.IMAGE_TYPE_MINI_CONTROLLER_THUMBNAIL:
                    case ImagePicker.IMAGE_TYPE_NOTIFICATION_THUMBNAIL:
                        return images.get(0);
                    default:
                        return images.get(1);
                }
            }
        }
    }
}
