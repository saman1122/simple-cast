package com.hencky.simplecast.utils;

import android.os.Build;

public class PlatformUtils {
    public static boolean isAtLeastO() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }
}
