package com.hencky.simplecast.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import com.hencky.simplecast.model.DisplayMode;
import com.hencky.simplecast.model.SortMode;
import com.hencky.simplecast.model.SortOrder;
import com.hencky.simplecast.model.SortType;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

public class PreferencesHelper {

    /**
     * Preferences
     */
    public static final String PREF_KEY_DISPLAY_MODE = "pref_display_mode";
    public static final String PREF_KEY_NIGHT_MODE = "pref_night_mode";
    public static final String PREF_KEY_SORT_MODE = "pref_sort_mode";
    public static final String PREF_KEY_ROOT_DIRECTORY = "pref_root_directory";
    public static final String PREF_KEY_BACKWARD_TIME = "pref_backward_time";
    public static final String PREF_KEY_FORWARD_TIME = "pref_forward_time";

    private static PreferencesHelper mInstance;

    private final SharedPreferences mPrefs;

    public static synchronized PreferencesHelper getInstance(@NonNull Context context) {
        if (mInstance == null) {
            mInstance = new PreferencesHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    private PreferencesHelper(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Gets the display mode in pref_general.
     *
     * @return The display mode
     */
    public DisplayMode getDisplayMode() {
        return DisplayMode.valueOf(mPrefs.getString(PREF_KEY_DISPLAY_MODE, DisplayMode.GRID.name()));
    }

    /**
     * Returns whether the night mode was enabled or not.
     *
     * @return {@code true} whether the night mode is enabled, otherwise {@code false}
     */
    public boolean isNightModeEnabled() {
        return mPrefs.getBoolean(PREF_KEY_NIGHT_MODE, false);
    }

    /**
     * Gets the sort mode in pref_general.
     *
     * @return The sort mode
     */
    public SortMode getSortMode() {
        String sortModePref = mPrefs.getString(PREF_KEY_SORT_MODE, "DATE_DESC");
        String[] result = sortModePref.split("_");
        return new SortMode(SortType.valueOf(result[0]), SortOrder.valueOf(result[1]));
    }

    public String getRootDirectory() {
        String rootDirectory = mPrefs.getString(PREF_KEY_ROOT_DIRECTORY,
                Environment.getExternalStorageDirectory()
                        .getPath());
        return rootDirectory.endsWith(":") ? rootDirectory.substring(0,
                rootDirectory.indexOf(":")) : rootDirectory;
    }

    /**
     * Saves the root directory in pref_general.
     *
     * @param path the path to save as root directory
     * @return {@code true} if the root directory has been successfully saved in pref_general,
     * otherwise {@code false}.
     */
    public boolean setRootDirectory(String path) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_ROOT_DIRECTORY, path);
        return editor.commit();
    }

    /**
     * Gets the backward time in pref_general.
     *
     * @return The backward time in seconds
     */
    public int getBackwardTimeInSeconds() {
        return Integer.valueOf(mPrefs.getString(PREF_KEY_BACKWARD_TIME, "30"));
    }

    /**
     * Gets the forward time in pref_general.
     *
     * @return The forward time in seconds
     */
    public int getForwardTimeInSeconds() {
        return Integer.valueOf(mPrefs.getString(PREF_KEY_FORWARD_TIME, "30"));
    }
}
