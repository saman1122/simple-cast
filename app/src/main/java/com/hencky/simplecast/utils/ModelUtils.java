package com.hencky.simplecast.utils;

import com.google.android.gms.cast.CastDevice;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;

/**
 * Provides utilities for model manipulation.
 */
public class ModelUtils {

    /**
     * Converts a UPnP receiver device to a {@link Device}.
     *
     * @param upnpDevice the UPnP receiver device to convert
     * @return the converted Device
     */
    public static Device toDevice(org.fourthline.cling.model.meta.Device upnpDevice) {
        if (upnpDevice == null)
            return null;

        String name = upnpDevice.getDetails() != null &&
                upnpDevice.getDetails().getFriendlyName() != null
                ? upnpDevice.getDetails().getFriendlyName() : upnpDevice.getDisplayString();
        String identifier = upnpDevice.getIdentity().getUdn().getIdentifierString();
        Device device = new Device();
        device.setIdentifier(identifier);
        // Display a little star while the device is being loaded
        device.setName(upnpDevice.isFullyHydrated() ? name : name + " *");
        device.setDeviceType(DeviceType.UPNP);
        return device;
    }

    /**
     * Converts a Cast receiver device to a {@link Device}.
     *
     * @param castDevice the Cast receiver device to convert
     * @return the converted Device
     */
    public static Device toDevice(CastDevice castDevice) {
        if (castDevice == null)
            return null;
        Device device = new Device();
        device.setIdentifier(castDevice.getDeviceId());
        device.setName(castDevice.getFriendlyName());
        device.setDeviceType(DeviceType.CAST);
        return device;
    }
}
