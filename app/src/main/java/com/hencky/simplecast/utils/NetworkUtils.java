package com.hencky.simplecast.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.net.InetAddress;
import java.net.UnknownHostException;

import androidx.annotation.NonNull;

/**
 * This class provides network utilities.
 */
public class NetworkUtils {

    /**
     * Returns the Wifi IP address.
     *
     * @param context the context to use
     * @return the Wifi IP address
     */
    public static String getWifiIPAddress(Context context) {
        WifiManager manager = (WifiManager) context.getApplicationContext().getSystemService(
                Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        int address = info.getIpAddress();
        return intToInetAddress(address).getHostAddress();
    }

    /**
     * Convert a IPv4 address from an integer to an {@link InetAddress}.
     *
     * @param hostAddress an int corresponding to the IPv4 address in network byte order
     */
    private static InetAddress intToInetAddress(int hostAddress) {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};
        try {
            return InetAddress.getByAddress(addressBytes);
        } catch (UnknownHostException e) {
            throw new AssertionError();
        }
    }

    /**
     * Checks if we are connected to the WiFi network.
     *
     * @return {@code true} whether we are connected to the WiFi network, otherwise {@code false}.
     */
    public static boolean isWiFiNetworkConnected(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null)
            return false;
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(
                activeNetwork);
        return networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) &&
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
    }
}
