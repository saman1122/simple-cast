package com.hencky.simplecast.utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

/**
 * Provides utilities for drawable operations.
 */
public class DrawableUtils {

    /**
     * Tints a drawable for a given color.
     *
     * @param context       The context to inflate against
     * @param drawableResId The desired resource identifier for the drawable
     * @param colorResId    The resource identifier for the tint color to apply to the drawable
     * @return The tinted drawable
     */
    public static Drawable tintDrawable(@NonNull Context context, @DrawableRes int drawableResId,
                                        @ColorRes int colorResId) {
        Drawable d;
        DrawableCompat.setTintMode(d = DrawableCompat.wrap(AppCompatResources.getDrawable(context,
                drawableResId)
                .mutate()), PorterDuff.Mode.SRC_IN);
        if (colorResId != 0) {
            DrawableCompat.setTint(d, ContextCompat.getColor(context, colorResId));
        }
        return d;
    }

    /**
     * Tints a {@link Drawable}.
     *
     * @param context    the Context to use
     * @param drawable   the Drawable to tint
     * @param colorResId the resource identifier for the color to apply
     * @return the tinted Drawable
     */
    public static Drawable tintDrawable(@NonNull Context context, @NonNull Drawable drawable,
                                        @ColorRes int colorResId) {
        return tintDrawable(drawable, ContextCompat.getColor(context, colorResId));
    }

    /**
     * Tints a {@link Drawable}.
     *
     * @param drawable the Drawable to tint
     * @param color    the color to apply to the icon drawable
     * @return the tinted Drawable
     */
    public static Drawable tintDrawable(@NonNull Drawable drawable, @ColorInt int color) {
        // need to mutate otherwise all references to this drawable will be tinted
        Drawable mutableDrawable = drawable.mutate();
        Drawable wrappedDrawable = DrawableCompat.wrap(mutableDrawable);
        DrawableCompat.setTint(wrappedDrawable, color);
        return wrappedDrawable;
    }

    /**
     * Sets a compound drawable to the left of a TextView.
     *
     * @param context       The context to inflate against
     * @param textView      The TextView where to set the compound drawable
     * @param drawableResId The desired resource identifier for the drawable
     * @param colorResId    The resource identifier for the tint color to apply to the drawable
     */
    public static void setCompoundDrawableTop(@NonNull Context context, @NonNull TextView textView,
                                              @DrawableRes int drawableResId,
                                              @ColorRes int colorResId) {
        Drawable d = AppCompatResources.getDrawable(context, drawableResId).mutate();
        Drawable w = DrawableCompat.wrap(d);
        if (colorResId != 0) {
            DrawableCompat.setTint(w, ContextCompat.getColor(context, colorResId));
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(null, w, null, null);
    }
}
