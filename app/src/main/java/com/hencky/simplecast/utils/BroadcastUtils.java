package com.hencky.simplecast.utils;

import android.content.Context;
import android.content.Intent;

import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.ActionInvocationResult;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/**
 * This class provides utilities for broadcasting messages.
 */
public class BroadcastUtils {

    /**
     * Broadcasts an intent for a given ActionInvocation result
     * to all interested BroadcastReceivers.
     *
     * @param context The context to use.
     * @param action  The action that was invoked.
     * @param result  The result of the action invocation.
     * @param message The optional message that can be associated to the result.
     */
    public static void broadcastActionInvocationResultIntent(@NonNull Context context,
                                                             String action,
                                                             ActionInvocationResult result,
                                                             @Nullable String message) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putExtra(AppKeys.EXTRA_ACTION_INVOCATION_RESULT, result);
        intent.putExtra(AppKeys.EXTRA_ACTION_INVOCATION_RESULT_MESSAGE, message);
        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
    }
}
