package com.hencky.simplecast.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Provides utilities for date and time manipulation.
 */
public class DateTimeUtils {

    /**
     * Converts a millisecond duration to a string format.
     * The form of the duration string is HH:MM:SS where
     * HH: exactly 2 digits to indicate elapsed hours,
     * MM: exactly 2 digits to indicate minutes (00 to 59),
     * SS: exactly 2 digits to indicate seconds (00 to 59),
     *
     * @param millis the duration to convert to a string form
     * @return the duration string
     */
    public static String toDurationString(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long hours = TimeUnit.MILLISECONDS.toHours(millis) % 24;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;

        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds);
    }

    /**
     * Converts an epoch time in seconds to a datetime string whose format is
     * "yyyy-MM-dd, HH:mm:ss".
     *
     * @param epochTimeInSeconds epoch time in seconds
     * @return the formatted datetime
     */
    public static String toDateTimeString(long epochTimeInSeconds) {
        Date d = new Date(epochTimeInSeconds * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
        return sdf.format(d);
    }
}
