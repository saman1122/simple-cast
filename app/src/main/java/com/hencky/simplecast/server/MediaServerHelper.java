package com.hencky.simplecast.server;

import android.content.Context;

import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.utils.LogHelper;

import androidx.annotation.NonNull;

/**
 * A helper class to interact with media server.
 */
public class MediaServerHelper {

    private static final String TAG = LogHelper.makeLogTag(MediaServerHelper.class);

    private static MediaServerHelper INSTANCE;

    private final Context mAppContext;
    private MediaServer mServer;

    private MediaServerHelper(Context appContext) {
        mAppContext = appContext;
    }

    public static synchronized MediaServerHelper getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new MediaServerHelper(context.getApplicationContext());
        }

        return INSTANCE;
    }

    /**
     * Starts the media server.
     */
    public void startMediaServer() {
        if (mServer == null) {
            mServer = new MediaServer(mAppContext);
            mServer.start();
        } else {
            LogHelper.w(TAG, "The Media server is already started.");
        }
    }

    /**
     * Stops the media server.
     */
    public void stopMediaServer() {
        if (mServer == null) {
            LogHelper.w(TAG, "The Media server is already stopped.");
        } else {
            mServer.stop();
            mServer = null;
        }
    }

    /**
     * Sets up the Media server for a given {@link MediaItem}.
     *
     * @param mediaItem The media item to set
     */
    public void setupMediaServer(@NonNull MediaItem mediaItem) {
        if (mServer == null)
            return;

        mServer.setMediaItem(mediaItem);
    }

    /**
     * Releases resources.
     */
    public void release() {
        stopMediaServer();
        INSTANCE = null;
    }
}
