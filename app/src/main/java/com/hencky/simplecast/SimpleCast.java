package com.hencky.simplecast;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.hencky.simplecast.utils.PreferencesHelper;

import org.fourthline.cling.android.FixedAndroidLogHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

import androidx.appcompat.app.AppCompatDelegate;
import io.fabric.sdk.android.Fabric;

/**
 * This application class.
 */
public class SimpleCast extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Fix the logging integration between java.util.logging and Android internal logging.
        org.seamless.util.logging.LoggingUtil.resetRootHandler(new FixedAndroidLogHandler());

        // Now you can enable logging as needed for various categories of Cling.
        Logger.getLogger("org.fourthline.cling").setLevel(Level.INFO);

        // Initialize Fabric for crash reporting: Crashlytics.
        Fabric.with(getApplicationContext(), new Crashlytics());

        // Set night mode.
        boolean nightModeEnabled = PreferencesHelper.getInstance(this).isNightModeEnabled();
        if (nightModeEnabled) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }
}
